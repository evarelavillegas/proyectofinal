﻿Public Class frmConsultaDatosPersonales

    Private Sub frmConsultaDatosPersonales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet1.DatosPersonales' table. You can move, or remove it, as needed.
        Me.DatosPersonalesTableAdapter1.Fill(Me.Planilla2DataSet1.DatosPersonales)
        'TODO: This line of code loads data into the 'Planilla2DataSet.DatosPersonales' table. You can move, or remove it, as needed.
        Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)

    End Sub

    Private Sub DatosPersonalesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.DatosPersonalesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub


    Sub buscarRegistros()
        '  DatosPersonalesTableAdapter1.FillByDataEmployee(Me.Planilla2DataSet1.DatosPersonales, Integer.Parse(txtDatoBusqueda.Text))

    End Sub

    Private Sub btnMostarRegistros_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMostarRegistros.Click
        Me.DatosPersonalesTableAdapter1.Fill(Me.Planilla2DataSet1.DatosPersonales)

    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Me.DatosPersonalesTableAdapter1.Fill(Me.Planilla2DataSet1.DatosPersonales)

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        buscarRegistros()
    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        Me.Close()
        frmPlanillaMensual.Show()
    End Sub

    Private Sub RegresarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegresarToolStripMenuItem.Click
        Me.Close()
        frmPlanillaMensual.Show()

    End Sub

    Private Sub DatosPersonalesDataGridView_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DatosPersonalesDataGridView.CellClick
        ToolStripStatusLabel1.Text = "Doble click para seleccionar un registro  de la lista"
        Try
            txtDatoBusqueda.Text = Me.DatosPersonalesDataGridView.Rows(e.RowIndex).Cells(0).Value()
            My.Forms.frmPlanillaMensual.NoCEDULATextBox.Text = txtDatoBusqueda.Text

        Catch ex As Exception

        End Try
    End Sub

    Private Sub DatosPersonalesDataGridView_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DatosPersonalesDataGridView.MouseDoubleClick
        Me.Close()
        frmPlanillaMensual.Show()

    End Sub

    Private Sub DatosPersonalesDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DatosPersonalesDataGridView.CellContentClick

    End Sub

    Private Sub frmConsultaDatosPersonales_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        My.Forms.frmPlanillaMensual.NoCEDULATextBox.Text = txtDatoBusqueda.Text
    End Sub

    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class