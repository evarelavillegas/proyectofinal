﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPlanillaMensual
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim LIQUIDO_A_PAGARLabel As System.Windows.Forms.Label
        Dim TOTALAJUSTARLabel As System.Windows.Forms.Label
        Dim NoCEDULALabel As System.Windows.Forms.Label
        Dim NOMBRE_COMPLETOLabel As System.Windows.Forms.Label
        Dim E_MAILLabel As System.Windows.Forms.Label
        Dim CODPUESTOLabel As System.Windows.Forms.Label
        Dim AÑOSSERVLabel As System.Windows.Forms.Label
        Dim N__CONYUGESLabel As System.Windows.Forms.Label
        Dim N__HIJOSLabel As System.Windows.Forms.Label
        Dim InstitucionIncapacidadLabel As System.Windows.Forms.Label
        Dim TOTAL_DIAS_INCAPLabel As System.Windows.Forms.Label
        Dim FechaINCAPINILabel As System.Windows.Forms.Label
        Dim FechaINCAPFINLabel As System.Windows.Forms.Label
        Dim SUBSIDIOLabel As System.Windows.Forms.Label
        Dim CodImpuestoLabel As System.Windows.Forms.Label
        Dim ProcentajeImpuesto1Label As System.Windows.Forms.Label
        Dim ProcentajeImpuesto2Label As System.Windows.Forms.Label
        Dim CreditosFiscalesHijoLabel As System.Windows.Forms.Label
        Dim CreditosFiscalesConyugeLabel As System.Windows.Forms.Label
        Dim HORAS_EXTRAS_DOBLESLabel As System.Windows.Forms.Label
        Dim HORAS_EXTRAS_REGULARLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim CCSSLabel As System.Windows.Forms.Label
        Dim DIAS_TRABAJLabel As System.Windows.Forms.Label
        Dim SALARIO_DIARIOLabel As System.Windows.Forms.Label
        Dim CESANTIALabel As System.Windows.Forms.Label
        Dim IMP_RENTALabel As System.Windows.Forms.Label
        Dim SALARIO_UNICOLabel As System.Windows.Forms.Label
        Dim AsientoLabel As System.Windows.Forms.Label
        Dim ANUALIDADLabel As System.Windows.Forms.Label
        Dim __ANUALLabel As System.Windows.Forms.Label
        Dim FechaPlanMensualLabel As System.Windows.Forms.Label
        Dim SumaTotalLabel As System.Windows.Forms.Label
        Dim FECHA_INGRESOLabel1 As System.Windows.Forms.Label
        Me.Planilla2DataSet = New PlanillasProyecto3.Planilla2DataSet()
        Me.PlanillaMensualBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PlanillaMensualTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.PlanillaMensualTableAdapter()
        Me.TableAdapterManager = New PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton5 = New System.Windows.Forms.ToolStripButton()
        Me.StatusStrip3 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegresarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresaerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BuscarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevoRegistroToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanillaBisemanalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.PlanillaBisemanalTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.PlanillaBisemanalTableAdapter()
        Me.LIQUIDO_A_PAGARTextBox = New System.Windows.Forms.TextBox()
        Me.PlanillaMensualBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DatosPersonalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Planilla2DataSet1 = New PlanillasProyecto3.Planilla2DataSet1()
        Me.btnRegresar = New System.Windows.Forms.Button()
        Me.AjustesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.FECHA_INGRESODateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.btnMostar = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TOTALAJUSTARTextBox = New System.Windows.Forms.TextBox()
        Me.NoCEDULATextBox = New System.Windows.Forms.TextBox()
        Me.NOMBRE_COMPLETOTextBox = New System.Windows.Forms.TextBox()
        Me.E_MAILTextBox = New System.Windows.Forms.TextBox()
        Me.CODPUESTOTextBox = New System.Windows.Forms.TextBox()
        Me.N__CONYUGESCheckBox = New System.Windows.Forms.CheckBox()
        Me.N__HIJOSTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ComboInstitucion = New System.Windows.Forms.ComboBox()
        Me.FechaINCAPFINDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.FechaINCAPINIDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.SUBSIDIOTextBox = New System.Windows.Forms.TextBox()
        Me.TOTAL_DIAS_INCAPTextBox = New System.Windows.Forms.TextBox()
        Me.DeduccionesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CodImpuestoTextBox = New System.Windows.Forms.TextBox()
        Me.ImpuestosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcentajeImpuesto1TextBox = New System.Windows.Forms.TextBox()
        Me.ProcentajeImpuesto2TextBox = New System.Windows.Forms.TextBox()
        Me.CreditosFiscalesHijoTextBox = New System.Windows.Forms.TextBox()
        Me.CreditosFiscalesConyugeTextBox = New System.Windows.Forms.TextBox()
        Me.txtExtras = New System.Windows.Forms.TextBox()
        Me.CCSSTextBox = New System.Windows.Forms.TextBox()
        Me.SALARIO_DIARIOTextBox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.IMP_RENTATextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.SALARIO_UNICOTextBox1 = New System.Windows.Forms.TextBox()
        Me.PuestosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.comboxHorasExtrasDobles = New System.Windows.Forms.ComboBox()
        Me.horasExtrasRegulares = New System.Windows.Forms.ComboBox()
        Me.diazTrabajados = New System.Windows.Forms.ComboBox()
        Me.añoImpuestoFiscal = New System.Windows.Forms.TextBox()
        Me.CESANTIATextBox = New System.Windows.Forms.TextBox()
        Me.DatosPersonalesTableAdapter = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.DatosPersonalesTableAdapter()
        Me.TableAdapterManager1 = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.TableAdapterManager()
        Me.AjustesTableAdapter = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.AjustesTableAdapter()
        Me.PlanillaMensualTableAdapter1 = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.PlanillaMensualTableAdapter()
        Me.PuestosTableAdapter = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.PuestosTableAdapter()
        Me.DeduccionesTableAdapter = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.DeduccionesTableAdapter()
        Me.ImpuestosTableAdapter = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.ImpuestosTableAdapter()
        Me.PlanillaMensualDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.AsientoTextBox = New System.Windows.Forms.TextBox()
        Me.ANUALIDADTextBox = New System.Windows.Forms.TextBox()
        Me.__ANUALTextBox = New System.Windows.Forms.TextBox()
        Me.SumaTotalTextBox = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.FechaPlanMensualDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.PlanillaMensualDataGridView1 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn34 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn35 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn36 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn37 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn38 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn39 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn40 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn41 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn42 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn43 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn44 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn45 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn46 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        LIQUIDO_A_PAGARLabel = New System.Windows.Forms.Label()
        TOTALAJUSTARLabel = New System.Windows.Forms.Label()
        NoCEDULALabel = New System.Windows.Forms.Label()
        NOMBRE_COMPLETOLabel = New System.Windows.Forms.Label()
        E_MAILLabel = New System.Windows.Forms.Label()
        CODPUESTOLabel = New System.Windows.Forms.Label()
        AÑOSSERVLabel = New System.Windows.Forms.Label()
        N__CONYUGESLabel = New System.Windows.Forms.Label()
        N__HIJOSLabel = New System.Windows.Forms.Label()
        InstitucionIncapacidadLabel = New System.Windows.Forms.Label()
        TOTAL_DIAS_INCAPLabel = New System.Windows.Forms.Label()
        FechaINCAPINILabel = New System.Windows.Forms.Label()
        FechaINCAPFINLabel = New System.Windows.Forms.Label()
        SUBSIDIOLabel = New System.Windows.Forms.Label()
        CodImpuestoLabel = New System.Windows.Forms.Label()
        ProcentajeImpuesto1Label = New System.Windows.Forms.Label()
        ProcentajeImpuesto2Label = New System.Windows.Forms.Label()
        CreditosFiscalesHijoLabel = New System.Windows.Forms.Label()
        CreditosFiscalesConyugeLabel = New System.Windows.Forms.Label()
        HORAS_EXTRAS_DOBLESLabel = New System.Windows.Forms.Label()
        HORAS_EXTRAS_REGULARLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        CCSSLabel = New System.Windows.Forms.Label()
        DIAS_TRABAJLabel = New System.Windows.Forms.Label()
        SALARIO_DIARIOLabel = New System.Windows.Forms.Label()
        CESANTIALabel = New System.Windows.Forms.Label()
        IMP_RENTALabel = New System.Windows.Forms.Label()
        SALARIO_UNICOLabel = New System.Windows.Forms.Label()
        AsientoLabel = New System.Windows.Forms.Label()
        ANUALIDADLabel = New System.Windows.Forms.Label()
        __ANUALLabel = New System.Windows.Forms.Label()
        FechaPlanMensualLabel = New System.Windows.Forms.Label()
        SumaTotalLabel = New System.Windows.Forms.Label()
        FECHA_INGRESOLabel1 = New System.Windows.Forms.Label()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PlanillaMensualBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.StatusStrip3.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.PlanillaBisemanalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PlanillaMensualBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatosPersonalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Planilla2DataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AjustesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DeduccionesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImpuestosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.PuestosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PlanillaMensualDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.PlanillaMensualDataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LIQUIDO_A_PAGARLabel
        '
        LIQUIDO_A_PAGARLabel.AutoSize = True
        LIQUIDO_A_PAGARLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        LIQUIDO_A_PAGARLabel.Location = New System.Drawing.Point(81, 592)
        LIQUIDO_A_PAGARLabel.Name = "LIQUIDO_A_PAGARLabel"
        LIQUIDO_A_PAGARLabel.Size = New System.Drawing.Size(193, 24)
        LIQUIDO_A_PAGARLabel.TabIndex = 63
        LIQUIDO_A_PAGARLabel.Text = "LIQUIDO A PAGAR:"
        '
        'TOTALAJUSTARLabel
        '
        TOTALAJUSTARLabel.AutoSize = True
        TOTALAJUSTARLabel.Location = New System.Drawing.Point(78, 269)
        TOTALAJUSTARLabel.Name = "TOTALAJUSTARLabel"
        TOTALAJUSTARLabel.Size = New System.Drawing.Size(94, 13)
        TOTALAJUSTARLabel.TabIndex = 182
        TOTALAJUSTARLabel.Text = "TOTALAJUSTAR:"
        '
        'NoCEDULALabel
        '
        NoCEDULALabel.AutoSize = True
        NoCEDULALabel.Location = New System.Drawing.Point(95, 29)
        NoCEDULALabel.Name = "NoCEDULALabel"
        NoCEDULALabel.Size = New System.Drawing.Size(70, 13)
        NoCEDULALabel.TabIndex = 166
        NoCEDULALabel.Text = "No CEDULA:"
        '
        'NOMBRE_COMPLETOLabel
        '
        NOMBRE_COMPLETOLabel.AutoSize = True
        NOMBRE_COMPLETOLabel.Location = New System.Drawing.Point(54, 83)
        NOMBRE_COMPLETOLabel.Name = "NOMBRE_COMPLETOLabel"
        NOMBRE_COMPLETOLabel.Size = New System.Drawing.Size(119, 13)
        NOMBRE_COMPLETOLabel.TabIndex = 168
        NOMBRE_COMPLETOLabel.Text = "NOMBRE COMPLETO:"
        '
        'E_MAILLabel
        '
        E_MAILLabel.AutoSize = True
        E_MAILLabel.Location = New System.Drawing.Point(127, 109)
        E_MAILLabel.Name = "E_MAILLabel"
        E_MAILLabel.Size = New System.Drawing.Size(45, 13)
        E_MAILLabel.TabIndex = 170
        E_MAILLabel.Text = "E-MAIL:"
        '
        'CODPUESTOLabel
        '
        CODPUESTOLabel.AutoSize = True
        CODPUESTOLabel.Location = New System.Drawing.Point(95, 135)
        CODPUESTOLabel.Name = "CODPUESTOLabel"
        CODPUESTOLabel.Size = New System.Drawing.Size(77, 13)
        CODPUESTOLabel.TabIndex = 172
        CODPUESTOLabel.Text = "CODPUESTO:"
        '
        'AÑOSSERVLabel
        '
        AÑOSSERVLabel.AutoSize = True
        AÑOSSERVLabel.Location = New System.Drawing.Point(103, 187)
        AÑOSSERVLabel.Name = "AÑOSSERVLabel"
        AÑOSSERVLabel.Size = New System.Drawing.Size(69, 13)
        AÑOSSERVLabel.TabIndex = 176
        AÑOSSERVLabel.Text = "AÑOSSERV:"
        '
        'N__CONYUGESLabel
        '
        N__CONYUGESLabel.AutoSize = True
        N__CONYUGESLabel.Location = New System.Drawing.Point(87, 215)
        N__CONYUGESLabel.Name = "N__CONYUGESLabel"
        N__CONYUGESLabel.Size = New System.Drawing.Size(85, 13)
        N__CONYUGESLabel.TabIndex = 178
        N__CONYUGESLabel.Text = "N° CONYUGES:"
        '
        'N__HIJOSLabel
        '
        N__HIJOSLabel.AutoSize = True
        N__HIJOSLabel.Location = New System.Drawing.Point(116, 247)
        N__HIJOSLabel.Name = "N__HIJOSLabel"
        N__HIJOSLabel.Size = New System.Drawing.Size(56, 13)
        N__HIJOSLabel.TabIndex = 180
        N__HIJOSLabel.Text = "N° HIJOS:"
        '
        'InstitucionIncapacidadLabel
        '
        InstitucionIncapacidadLabel.AutoSize = True
        InstitucionIncapacidadLabel.Location = New System.Drawing.Point(33, 40)
        InstitucionIncapacidadLabel.Name = "InstitucionIncapacidadLabel"
        InstitucionIncapacidadLabel.Size = New System.Drawing.Size(120, 13)
        InstitucionIncapacidadLabel.TabIndex = 129
        InstitucionIncapacidadLabel.Text = "Institucion Incapacidad:"
        '
        'TOTAL_DIAS_INCAPLabel
        '
        TOTAL_DIAS_INCAPLabel.AutoSize = True
        TOTAL_DIAS_INCAPLabel.Location = New System.Drawing.Point(45, 115)
        TOTAL_DIAS_INCAPLabel.Name = "TOTAL_DIAS_INCAPLabel"
        TOTAL_DIAS_INCAPLabel.Size = New System.Drawing.Size(108, 13)
        TOTAL_DIAS_INCAPLabel.TabIndex = 137
        TOTAL_DIAS_INCAPLabel.Text = "TOTAL DIAS INCAP:"
        '
        'FechaINCAPINILabel
        '
        FechaINCAPINILabel.AutoSize = True
        FechaINCAPINILabel.Location = New System.Drawing.Point(64, 65)
        FechaINCAPINILabel.Name = "FechaINCAPINILabel"
        FechaINCAPINILabel.Size = New System.Drawing.Size(89, 13)
        FechaINCAPINILabel.TabIndex = 139
        FechaINCAPINILabel.Text = "Fecha INCAPINI:"
        '
        'FechaINCAPFINLabel
        '
        FechaINCAPFINLabel.AutoSize = True
        FechaINCAPFINLabel.Location = New System.Drawing.Point(61, 91)
        FechaINCAPFINLabel.Name = "FechaINCAPFINLabel"
        FechaINCAPFINLabel.Size = New System.Drawing.Size(92, 13)
        FechaINCAPFINLabel.TabIndex = 141
        FechaINCAPFINLabel.Text = "Fecha INCAPFIN:"
        '
        'SUBSIDIOLabel
        '
        SUBSIDIOLabel.AutoSize = True
        SUBSIDIOLabel.Location = New System.Drawing.Point(92, 145)
        SUBSIDIOLabel.Name = "SUBSIDIOLabel"
        SUBSIDIOLabel.Size = New System.Drawing.Size(61, 13)
        SUBSIDIOLabel.TabIndex = 147
        SUBSIDIOLabel.Text = "SUBSIDIO:"
        '
        'CodImpuestoLabel
        '
        CodImpuestoLabel.AutoSize = True
        CodImpuestoLabel.Location = New System.Drawing.Point(963, 546)
        CodImpuestoLabel.Name = "CodImpuestoLabel"
        CodImpuestoLabel.Size = New System.Drawing.Size(74, 13)
        CodImpuestoLabel.TabIndex = 171
        CodImpuestoLabel.Text = "cod Impuesto:"
        '
        'ProcentajeImpuesto1Label
        '
        ProcentajeImpuesto1Label.AutoSize = True
        ProcentajeImpuesto1Label.Location = New System.Drawing.Point(963, 572)
        ProcentajeImpuesto1Label.Name = "ProcentajeImpuesto1Label"
        ProcentajeImpuesto1Label.Size = New System.Drawing.Size(112, 13)
        ProcentajeImpuesto1Label.TabIndex = 173
        ProcentajeImpuesto1Label.Text = "procentaje Impuesto1:"
        '
        'ProcentajeImpuesto2Label
        '
        ProcentajeImpuesto2Label.AutoSize = True
        ProcentajeImpuesto2Label.Location = New System.Drawing.Point(963, 598)
        ProcentajeImpuesto2Label.Name = "ProcentajeImpuesto2Label"
        ProcentajeImpuesto2Label.Size = New System.Drawing.Size(112, 13)
        ProcentajeImpuesto2Label.TabIndex = 175
        ProcentajeImpuesto2Label.Text = "procentaje Impuesto2:"
        '
        'CreditosFiscalesHijoLabel
        '
        CreditosFiscalesHijoLabel.AutoSize = True
        CreditosFiscalesHijoLabel.Location = New System.Drawing.Point(963, 624)
        CreditosFiscalesHijoLabel.Name = "CreditosFiscalesHijoLabel"
        CreditosFiscalesHijoLabel.Size = New System.Drawing.Size(109, 13)
        CreditosFiscalesHijoLabel.TabIndex = 177
        CreditosFiscalesHijoLabel.Text = "creditos Fiscales Hijo:"
        '
        'CreditosFiscalesConyugeLabel
        '
        CreditosFiscalesConyugeLabel.AutoSize = True
        CreditosFiscalesConyugeLabel.Location = New System.Drawing.Point(963, 650)
        CreditosFiscalesConyugeLabel.Name = "CreditosFiscalesConyugeLabel"
        CreditosFiscalesConyugeLabel.Size = New System.Drawing.Size(133, 13)
        CreditosFiscalesConyugeLabel.TabIndex = 179
        CreditosFiscalesConyugeLabel.Text = "creditos Fiscales Conyuge:"
        '
        'HORAS_EXTRAS_DOBLESLabel
        '
        HORAS_EXTRAS_DOBLESLabel.AutoSize = True
        HORAS_EXTRAS_DOBLESLabel.Location = New System.Drawing.Point(19, 237)
        HORAS_EXTRAS_DOBLESLabel.Name = "HORAS_EXTRAS_DOBLESLabel"
        HORAS_EXTRAS_DOBLESLabel.Size = New System.Drawing.Size(140, 13)
        HORAS_EXTRAS_DOBLESLabel.TabIndex = 153
        HORAS_EXTRAS_DOBLESLabel.Text = "HORAS EXTRAS DOBLES:"
        '
        'HORAS_EXTRAS_REGULARLabel
        '
        HORAS_EXTRAS_REGULARLabel.AutoSize = True
        HORAS_EXTRAS_REGULARLabel.Location = New System.Drawing.Point(12, 211)
        HORAS_EXTRAS_REGULARLabel.Name = "HORAS_EXTRAS_REGULARLabel"
        HORAS_EXTRAS_REGULARLabel.Size = New System.Drawing.Size(149, 13)
        HORAS_EXTRAS_REGULARLabel.TabIndex = 151
        HORAS_EXTRAS_REGULARLabel.Text = "HORAS EXTRAS REGULAR:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(6, 263)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(204, 13)
        Label1.TabIndex = 155
        Label1.Text = "Total a pagar por jornada normal + Extras:"
        '
        'CCSSLabel
        '
        CCSSLabel.AutoSize = True
        CCSSLabel.Location = New System.Drawing.Point(123, 74)
        CCSSLabel.Name = "CCSSLabel"
        CCSSLabel.Size = New System.Drawing.Size(38, 13)
        CCSSLabel.TabIndex = 159
        CCSSLabel.Text = "CCSS:"
        '
        'DIAS_TRABAJLabel
        '
        DIAS_TRABAJLabel.AutoSize = True
        DIAS_TRABAJLabel.Location = New System.Drawing.Point(80, 156)
        DIAS_TRABAJLabel.Name = "DIAS_TRABAJLabel"
        DIAS_TRABAJLabel.Size = New System.Drawing.Size(79, 13)
        DIAS_TRABAJLabel.TabIndex = 149
        DIAS_TRABAJLabel.Text = "DIAS TRABAJ:"
        '
        'SALARIO_DIARIOLabel
        '
        SALARIO_DIARIOLabel.AutoSize = True
        SALARIO_DIARIOLabel.Location = New System.Drawing.Point(65, 48)
        SALARIO_DIARIOLabel.Name = "SALARIO_DIARIOLabel"
        SALARIO_DIARIOLabel.Size = New System.Drawing.Size(96, 13)
        SALARIO_DIARIOLabel.TabIndex = 145
        SALARIO_DIARIOLabel.Text = "SALARIO DIARIO:"
        '
        'CESANTIALabel
        '
        CESANTIALabel.AutoSize = True
        CESANTIALabel.Location = New System.Drawing.Point(96, 180)
        CESANTIALabel.Name = "CESANTIALabel"
        CESANTIALabel.Size = New System.Drawing.Size(63, 13)
        CESANTIALabel.TabIndex = 157
        CESANTIALabel.Text = "CESANTIA:"
        '
        'IMP_RENTALabel
        '
        IMP_RENTALabel.AutoSize = True
        IMP_RENTALabel.Location = New System.Drawing.Point(93, 131)
        IMP_RENTALabel.Name = "IMP_RENTALabel"
        IMP_RENTALabel.Size = New System.Drawing.Size(69, 13)
        IMP_RENTALabel.TabIndex = 193
        IMP_RENTALabel.Text = "IMP RENTA:"
        '
        'SALARIO_UNICOLabel
        '
        SALARIO_UNICOLabel.AutoSize = True
        SALARIO_UNICOLabel.Location = New System.Drawing.Point(60, 22)
        SALARIO_UNICOLabel.Name = "SALARIO_UNICOLabel"
        SALARIO_UNICOLabel.Size = New System.Drawing.Size(93, 13)
        SALARIO_UNICOLabel.TabIndex = 196
        SALARIO_UNICOLabel.Text = "SALARIO UNICO:"
        '
        'AsientoLabel
        '
        AsientoLabel.AutoSize = True
        AsientoLabel.Location = New System.Drawing.Point(99, 106)
        AsientoLabel.Name = "AsientoLabel"
        AsientoLabel.Size = New System.Drawing.Size(45, 13)
        AsientoLabel.TabIndex = 137
        AsientoLabel.Text = "Asiento:"
        '
        'ANUALIDADLabel
        '
        ANUALIDADLabel.AutoSize = True
        ANUALIDADLabel.Location = New System.Drawing.Point(74, 82)
        ANUALIDADLabel.Name = "ANUALIDADLabel"
        ANUALIDADLabel.Size = New System.Drawing.Size(72, 13)
        ANUALIDADLabel.TabIndex = 113
        ANUALIDADLabel.Text = "ANUALIDAD:"
        '
        '__ANUALLabel
        '
        __ANUALLabel.AutoSize = True
        __ANUALLabel.Location = New System.Drawing.Point(87, 56)
        __ANUALLabel.Name = "__ANUALLabel"
        __ANUALLabel.Size = New System.Drawing.Size(57, 13)
        __ANUALLabel.TabIndex = 111
        __ANUALLabel.Text = "% ANUAL:"
        '
        'FechaPlanMensualLabel
        '
        FechaPlanMensualLabel.AutoSize = True
        FechaPlanMensualLabel.Location = New System.Drawing.Point(39, 29)
        FechaPlanMensualLabel.Name = "FechaPlanMensualLabel"
        FechaPlanMensualLabel.Size = New System.Drawing.Size(107, 13)
        FechaPlanMensualLabel.TabIndex = 105
        FechaPlanMensualLabel.Text = "Fecha Plan Mensual:"
        '
        'SumaTotalLabel
        '
        SumaTotalLabel.AutoSize = True
        SumaTotalLabel.Location = New System.Drawing.Point(18, 161)
        SumaTotalLabel.Name = "SumaTotalLabel"
        SumaTotalLabel.Size = New System.Drawing.Size(130, 13)
        SumaTotalLabel.TabIndex = 149
        SumaTotalLabel.Text = "Suma Total Deducciones:"
        '
        'FECHA_INGRESOLabel1
        '
        FECHA_INGRESOLabel1.AutoSize = True
        FECHA_INGRESOLabel1.Location = New System.Drawing.Point(75, 160)
        FECHA_INGRESOLabel1.Name = "FECHA_INGRESOLabel1"
        FECHA_INGRESOLabel1.Size = New System.Drawing.Size(97, 13)
        FECHA_INGRESOLabel1.TabIndex = 190
        FECHA_INGRESOLabel1.Text = "FECHA INGRESO:"
        '
        'Planilla2DataSet
        '
        Me.Planilla2DataSet.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PlanillaMensualBindingSource
        '
        Me.PlanillaMensualBindingSource.DataMember = "PlanillaMensual"
        Me.PlanillaMensualBindingSource.DataSource = Me.Planilla2DataSet
        '
        'PlanillaMensualTableAdapter
        '
        Me.PlanillaMensualTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AguinaldoTableAdapter = Nothing
        Me.TableAdapterManager.AjustesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Nothing
        Me.TableAdapterManager.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager.ImpuestosTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Me.PlanillaMensualTableAdapter
        Me.TableAdapterManager.PuestosTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UsuarioRegistroTableAdapter = Nothing
        Me.TableAdapterManager.VacacionesTableAdapter = Nothing
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripButton3, Me.ToolStripButton4, Me.ToolStripButton5})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1306, 25)
        Me.ToolStrip1.TabIndex = 50
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = Global.PlanillasProyecto3.My.Resources.Resources.iconoBuscarAutor
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "Buscar empleado"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton3.Image = Global.PlanillasProyecto3.My.Resources.Resources.guardar_documento_icono_7840_48
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton3.Text = "Guardar"
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton4.Image = Global.PlanillasProyecto3.My.Resources.Resources.Eliminar
        Me.ToolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton4.Text = "ToolStripButton4"
        '
        'ToolStripButton5
        '
        Me.ToolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton5.Image = Global.PlanillasProyecto3.My.Resources.Resources.icono_retroceso
        Me.ToolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton5.Name = "ToolStripButton5"
        Me.ToolStripButton5.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton5.Text = "Regresar"
        '
        'StatusStrip3
        '
        Me.StatusStrip3.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStrip3.Location = New System.Drawing.Point(0, 684)
        Me.StatusStrip3.Name = "StatusStrip3"
        Me.StatusStrip3.Size = New System.Drawing.Size(1306, 22)
        Me.StatusStrip3.TabIndex = 51
        Me.StatusStrip3.Text = "StatusStrip3"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(39, 17)
        Me.ToolStripStatusLabel1.Text = "Status"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivoToolStripMenuItem, Me.AccionesToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1306, 24)
        Me.MenuStrip1.TabIndex = 52
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ArchivoToolStripMenuItem
        '
        Me.ArchivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegresarToolStripMenuItem})
        Me.ArchivoToolStripMenuItem.Name = "ArchivoToolStripMenuItem"
        Me.ArchivoToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.ArchivoToolStripMenuItem.Text = "&Archivo"
        '
        'RegresarToolStripMenuItem
        '
        Me.RegresarToolStripMenuItem.Name = "RegresarToolStripMenuItem"
        Me.RegresarToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F), System.Windows.Forms.Keys)
        Me.RegresarToolStripMenuItem.Size = New System.Drawing.Size(159, 22)
        Me.RegresarToolStripMenuItem.Text = "&Regresar"
        '
        'AccionesToolStripMenuItem
        '
        Me.AccionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresaerToolStripMenuItem, Me.BuscarToolStripMenuItem, Me.ModificarToolStripMenuItem, Me.NuevoRegistroToolStripMenuItem, Me.ConsultaToolStripMenuItem})
        Me.AccionesToolStripMenuItem.Name = "AccionesToolStripMenuItem"
        Me.AccionesToolStripMenuItem.Size = New System.Drawing.Size(67, 20)
        Me.AccionesToolStripMenuItem.Text = "&Acciones"
        '
        'IngresaerToolStripMenuItem
        '
        Me.IngresaerToolStripMenuItem.Name = "IngresaerToolStripMenuItem"
        Me.IngresaerToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
        Me.IngresaerToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.IngresaerToolStripMenuItem.Text = "&Ingresar"
        '
        'BuscarToolStripMenuItem
        '
        Me.BuscarToolStripMenuItem.Name = "BuscarToolStripMenuItem"
        Me.BuscarToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.K), System.Windows.Forms.Keys)
        Me.BuscarToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.BuscarToolStripMenuItem.Text = "&Buscar"
        '
        'ModificarToolStripMenuItem
        '
        Me.ModificarToolStripMenuItem.Name = "ModificarToolStripMenuItem"
        Me.ModificarToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.B), System.Windows.Forms.Keys)
        Me.ModificarToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.ModificarToolStripMenuItem.Text = "&Modificar"
        '
        'NuevoRegistroToolStripMenuItem
        '
        Me.NuevoRegistroToolStripMenuItem.Name = "NuevoRegistroToolStripMenuItem"
        Me.NuevoRegistroToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.NuevoRegistroToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.NuevoRegistroToolStripMenuItem.Text = "&Nuevo Registro"
        '
        'ConsultaToolStripMenuItem
        '
        Me.ConsultaToolStripMenuItem.Name = "ConsultaToolStripMenuItem"
        Me.ConsultaToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F17), System.Windows.Forms.Keys)
        Me.ConsultaToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.ConsultaToolStripMenuItem.Text = "&Consulta"
        '
        'PlanillaBisemanalBindingSource
        '
        Me.PlanillaBisemanalBindingSource.DataMember = "PlanillaBisemanal"
        Me.PlanillaBisemanalBindingSource.DataSource = Me.Planilla2DataSet
        '
        'btnModificar
        '
        Me.btnModificar.Image = Global.PlanillasProyecto3.My.Resources.Resources.Eliminar
        Me.btnModificar.Location = New System.Drawing.Point(570, 539)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(66, 47)
        Me.btnModificar.TabIndex = 61
        Me.ToolTip1.SetToolTip(Me.btnModificar, "Eliminar Registros")
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Image = Global.PlanillasProyecto3.My.Resources.Resources.guardar_documento_icono_7840_48
        Me.btnGuardar.Location = New System.Drawing.Point(492, 539)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(66, 47)
        Me.btnGuardar.TabIndex = 59
        Me.ToolTip1.SetToolTip(Me.btnGuardar, "Guardar Registros")
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'PlanillaBisemanalTableAdapter
        '
        Me.PlanillaBisemanalTableAdapter.ClearBeforeFill = True
        '
        'LIQUIDO_A_PAGARTextBox
        '
        Me.LIQUIDO_A_PAGARTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource1, "LIQUIDO A PAGAR", True))
        Me.LIQUIDO_A_PAGARTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LIQUIDO_A_PAGARTextBox.Location = New System.Drawing.Point(280, 592)
        Me.LIQUIDO_A_PAGARTextBox.Name = "LIQUIDO_A_PAGARTextBox"
        Me.LIQUIDO_A_PAGARTextBox.Size = New System.Drawing.Size(567, 26)
        Me.LIQUIDO_A_PAGARTextBox.TabIndex = 64
        '
        'PlanillaMensualBindingSource1
        '
        Me.PlanillaMensualBindingSource1.DataMember = "DatosPersonalesPlanillaMensual"
        Me.PlanillaMensualBindingSource1.DataSource = Me.DatosPersonalesBindingSource
        '
        'DatosPersonalesBindingSource
        '
        Me.DatosPersonalesBindingSource.DataMember = "DatosPersonales"
        Me.DatosPersonalesBindingSource.DataSource = Me.Planilla2DataSet1
        '
        'Planilla2DataSet1
        '
        Me.Planilla2DataSet1.DataSetName = "Planilla2DataSet1"
        Me.Planilla2DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'btnRegresar
        '
        Me.btnRegresar.Image = Global.PlanillasProyecto3.My.Resources.Resources.icono_retroceso
        Me.btnRegresar.Location = New System.Drawing.Point(722, 539)
        Me.btnRegresar.Name = "btnRegresar"
        Me.btnRegresar.Size = New System.Drawing.Size(66, 47)
        Me.btnRegresar.TabIndex = 65
        Me.btnRegresar.UseVisualStyleBackColor = True
        '
        'AjustesBindingSource
        '
        Me.AjustesBindingSource.DataMember = "DatosPersonalesAjustes"
        Me.AjustesBindingSource.DataSource = Me.DatosPersonalesBindingSource
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(FECHA_INGRESOLabel1)
        Me.GroupBox1.Controls.Add(Me.FECHA_INGRESODateTimePicker)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.RadioButton2)
        Me.GroupBox1.Controls.Add(Me.RadioButton1)
        Me.GroupBox1.Controls.Add(Me.btnMostar)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(TOTALAJUSTARLabel)
        Me.GroupBox1.Controls.Add(Me.TOTALAJUSTARTextBox)
        Me.GroupBox1.Controls.Add(NoCEDULALabel)
        Me.GroupBox1.Controls.Add(Me.NoCEDULATextBox)
        Me.GroupBox1.Controls.Add(NOMBRE_COMPLETOLabel)
        Me.GroupBox1.Controls.Add(Me.NOMBRE_COMPLETOTextBox)
        Me.GroupBox1.Controls.Add(E_MAILLabel)
        Me.GroupBox1.Controls.Add(Me.E_MAILTextBox)
        Me.GroupBox1.Controls.Add(CODPUESTOLabel)
        Me.GroupBox1.Controls.Add(Me.CODPUESTOTextBox)
        Me.GroupBox1.Controls.Add(AÑOSSERVLabel)
        Me.GroupBox1.Controls.Add(N__CONYUGESLabel)
        Me.GroupBox1.Controls.Add(Me.N__CONYUGESCheckBox)
        Me.GroupBox1.Controls.Add(N__HIJOSLabel)
        Me.GroupBox1.Controls.Add(Me.N__HIJOSTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(27, 52)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(411, 329)
        Me.GroupBox1.TabIndex = 166
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Empleado"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(14, 38)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 183
        Me.Button3.Text = "Button3"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'FECHA_INGRESODateTimePicker
        '
        Me.FECHA_INGRESODateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.DatosPersonalesBindingSource, "FECHA INGRESO", True))
        Me.FECHA_INGRESODateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FECHA_INGRESODateTimePicker.Location = New System.Drawing.Point(182, 158)
        Me.FECHA_INGRESODateTimePicker.Name = "FECHA_INGRESODateTimePicker"
        Me.FECHA_INGRESODateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.FECHA_INGRESODateTimePicker.TabIndex = 191
        '
        'TextBox4
        '
        Me.TextBox4.AcceptsReturn = True
        Me.TextBox4.Location = New System.Drawing.Point(185, 184)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(196, 20)
        Me.TextBox4.TabIndex = 190
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(266, 292)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(71, 17)
        Me.RadioButton2.TabIndex = 189
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Sin ajuste"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(185, 292)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(75, 17)
        Me.RadioButton1.TabIndex = 188
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Con ajuste"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'btnMostar
        '
        Me.btnMostar.Image = Global.PlanillasProyecto3.My.Resources.Resources.desktop
        Me.btnMostar.Location = New System.Drawing.Point(182, 51)
        Me.btnMostar.Name = "btnMostar"
        Me.btnMostar.Size = New System.Drawing.Size(88, 23)
        Me.btnMostar.TabIndex = 187
        Me.btnMostar.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Image = Global.PlanillasProyecto3.My.Resources.Resources.iconoBuscarAutor
        Me.Button1.Location = New System.Drawing.Point(276, 51)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(106, 22)
        Me.Button1.TabIndex = 184
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TOTALAJUSTARTextBox
        '
        Me.TOTALAJUSTARTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AjustesBindingSource, "TOTALAJUSTAR", True))
        Me.TOTALAJUSTARTextBox.Location = New System.Drawing.Point(181, 266)
        Me.TOTALAJUSTARTextBox.Name = "TOTALAJUSTARTextBox"
        Me.TOTALAJUSTARTextBox.Size = New System.Drawing.Size(200, 20)
        Me.TOTALAJUSTARTextBox.TabIndex = 183
        '
        'NoCEDULATextBox
        '
        Me.NoCEDULATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource1, "NoCEDULA", True))
        Me.NoCEDULATextBox.Location = New System.Drawing.Point(182, 28)
        Me.NoCEDULATextBox.Name = "NoCEDULATextBox"
        Me.NoCEDULATextBox.Size = New System.Drawing.Size(200, 20)
        Me.NoCEDULATextBox.TabIndex = 167
        '
        'NOMBRE_COMPLETOTextBox
        '
        Me.NOMBRE_COMPLETOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NOMBRE COMPLETO", True))
        Me.NOMBRE_COMPLETOTextBox.Location = New System.Drawing.Point(182, 80)
        Me.NOMBRE_COMPLETOTextBox.Name = "NOMBRE_COMPLETOTextBox"
        Me.NOMBRE_COMPLETOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.NOMBRE_COMPLETOTextBox.TabIndex = 169
        '
        'E_MAILTextBox
        '
        Me.E_MAILTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "E-MAIL", True))
        Me.E_MAILTextBox.Location = New System.Drawing.Point(182, 106)
        Me.E_MAILTextBox.Name = "E_MAILTextBox"
        Me.E_MAILTextBox.Size = New System.Drawing.Size(200, 20)
        Me.E_MAILTextBox.TabIndex = 171
        '
        'CODPUESTOTextBox
        '
        Me.CODPUESTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "CODPUESTO", True))
        Me.CODPUESTOTextBox.Location = New System.Drawing.Point(182, 132)
        Me.CODPUESTOTextBox.Name = "CODPUESTOTextBox"
        Me.CODPUESTOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.CODPUESTOTextBox.TabIndex = 173
        '
        'N__CONYUGESCheckBox
        '
        Me.N__CONYUGESCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.DatosPersonalesBindingSource, "N° CONYUGES", True))
        Me.N__CONYUGESCheckBox.Location = New System.Drawing.Point(182, 210)
        Me.N__CONYUGESCheckBox.Name = "N__CONYUGESCheckBox"
        Me.N__CONYUGESCheckBox.Size = New System.Drawing.Size(200, 24)
        Me.N__CONYUGESCheckBox.TabIndex = 179
        Me.N__CONYUGESCheckBox.Text = "Tiene pareja"
        Me.N__CONYUGESCheckBox.UseVisualStyleBackColor = True
        '
        'N__HIJOSTextBox
        '
        Me.N__HIJOSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "N° HIJOS", True))
        Me.N__HIJOSTextBox.Location = New System.Drawing.Point(182, 240)
        Me.N__HIJOSTextBox.Name = "N__HIJOSTextBox"
        Me.N__HIJOSTextBox.Size = New System.Drawing.Size(200, 20)
        Me.N__HIJOSTextBox.TabIndex = 181
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ComboInstitucion)
        Me.GroupBox2.Controls.Add(Me.FechaINCAPFINDateTimePicker)
        Me.GroupBox2.Controls.Add(Me.FechaINCAPINIDateTimePicker)
        Me.GroupBox2.Controls.Add(SUBSIDIOLabel)
        Me.GroupBox2.Controls.Add(Me.SUBSIDIOTextBox)
        Me.GroupBox2.Controls.Add(TOTAL_DIAS_INCAPLabel)
        Me.GroupBox2.Controls.Add(Me.TOTAL_DIAS_INCAPTextBox)
        Me.GroupBox2.Controls.Add(FechaINCAPINILabel)
        Me.GroupBox2.Controls.Add(FechaINCAPFINLabel)
        Me.GroupBox2.Controls.Add(InstitucionIncapacidadLabel)
        Me.GroupBox2.Location = New System.Drawing.Point(444, 353)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(432, 180)
        Me.GroupBox2.TabIndex = 169
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Incapacidad"
        '
        'ComboInstitucion
        '
        Me.ComboInstitucion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource1, "InstitucionIncapacidad", True))
        Me.ComboInstitucion.FormattingEnabled = True
        Me.ComboInstitucion.Items.AddRange(New Object() {"CCSS" & Global.Microsoft.VisualBasic.ChrW(9), "INS"})
        Me.ComboInstitucion.Location = New System.Drawing.Point(165, 38)
        Me.ComboInstitucion.Name = "ComboInstitucion"
        Me.ComboInstitucion.Size = New System.Drawing.Size(100, 21)
        Me.ComboInstitucion.TabIndex = 183
        '
        'FechaINCAPFINDateTimePicker
        '
        Me.FechaINCAPFINDateTimePicker.Checked = False
        Me.FechaINCAPFINDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.PlanillaMensualBindingSource1, "FechaINCAPFIN", True))
        Me.FechaINCAPFINDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaINCAPFINDateTimePicker.Location = New System.Drawing.Point(165, 86)
        Me.FechaINCAPFINDateTimePicker.Name = "FechaINCAPFINDateTimePicker"
        Me.FechaINCAPFINDateTimePicker.Size = New System.Drawing.Size(100, 20)
        Me.FechaINCAPFINDateTimePicker.TabIndex = 150
        '
        'FechaINCAPINIDateTimePicker
        '
        Me.FechaINCAPINIDateTimePicker.Checked = False
        Me.FechaINCAPINIDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.PlanillaMensualBindingSource1, "FechaINCAPINI", True))
        Me.FechaINCAPINIDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaINCAPINIDateTimePicker.Location = New System.Drawing.Point(165, 63)
        Me.FechaINCAPINIDateTimePicker.Name = "FechaINCAPINIDateTimePicker"
        Me.FechaINCAPINIDateTimePicker.Size = New System.Drawing.Size(100, 20)
        Me.FechaINCAPINIDateTimePicker.TabIndex = 149
        '
        'SUBSIDIOTextBox
        '
        Me.SUBSIDIOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource1, "SUBSIDIO", True))
        Me.SUBSIDIOTextBox.Location = New System.Drawing.Point(165, 141)
        Me.SUBSIDIOTextBox.Name = "SUBSIDIOTextBox"
        Me.SUBSIDIOTextBox.Size = New System.Drawing.Size(100, 20)
        Me.SUBSIDIOTextBox.TabIndex = 148
        '
        'TOTAL_DIAS_INCAPTextBox
        '
        Me.TOTAL_DIAS_INCAPTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource1, "TOTAL DIAS INCAP", True))
        Me.TOTAL_DIAS_INCAPTextBox.Location = New System.Drawing.Point(165, 112)
        Me.TOTAL_DIAS_INCAPTextBox.Name = "TOTAL_DIAS_INCAPTextBox"
        Me.TOTAL_DIAS_INCAPTextBox.Size = New System.Drawing.Size(100, 20)
        Me.TOTAL_DIAS_INCAPTextBox.TabIndex = 138
        '
        'DeduccionesBindingSource
        '
        Me.DeduccionesBindingSource.DataMember = "Deducciones"
        Me.DeduccionesBindingSource.DataSource = Me.Planilla2DataSet1
        '
        'CodImpuestoTextBox
        '
        Me.CodImpuestoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "codImpuesto", True))
        Me.CodImpuestoTextBox.Location = New System.Drawing.Point(1102, 532)
        Me.CodImpuestoTextBox.Name = "CodImpuestoTextBox"
        Me.CodImpuestoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CodImpuestoTextBox.TabIndex = 172
        '
        'ImpuestosBindingSource
        '
        Me.ImpuestosBindingSource.DataMember = "Impuestos"
        Me.ImpuestosBindingSource.DataSource = Me.Planilla2DataSet1
        '
        'ProcentajeImpuesto1TextBox
        '
        Me.ProcentajeImpuesto1TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "procentajeImpuesto1", True))
        Me.ProcentajeImpuesto1TextBox.Location = New System.Drawing.Point(1102, 569)
        Me.ProcentajeImpuesto1TextBox.Name = "ProcentajeImpuesto1TextBox"
        Me.ProcentajeImpuesto1TextBox.Size = New System.Drawing.Size(100, 20)
        Me.ProcentajeImpuesto1TextBox.TabIndex = 174
        '
        'ProcentajeImpuesto2TextBox
        '
        Me.ProcentajeImpuesto2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "procentajeImpuesto2", True))
        Me.ProcentajeImpuesto2TextBox.Location = New System.Drawing.Point(1102, 595)
        Me.ProcentajeImpuesto2TextBox.Name = "ProcentajeImpuesto2TextBox"
        Me.ProcentajeImpuesto2TextBox.Size = New System.Drawing.Size(100, 20)
        Me.ProcentajeImpuesto2TextBox.TabIndex = 176
        '
        'CreditosFiscalesHijoTextBox
        '
        Me.CreditosFiscalesHijoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "creditosFiscalesHijo", True))
        Me.CreditosFiscalesHijoTextBox.Location = New System.Drawing.Point(1102, 621)
        Me.CreditosFiscalesHijoTextBox.Name = "CreditosFiscalesHijoTextBox"
        Me.CreditosFiscalesHijoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CreditosFiscalesHijoTextBox.TabIndex = 178
        '
        'CreditosFiscalesConyugeTextBox
        '
        Me.CreditosFiscalesConyugeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "creditosFiscalesConyuge", True))
        Me.CreditosFiscalesConyugeTextBox.Location = New System.Drawing.Point(1102, 647)
        Me.CreditosFiscalesConyugeTextBox.Name = "CreditosFiscalesConyugeTextBox"
        Me.CreditosFiscalesConyugeTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CreditosFiscalesConyugeTextBox.TabIndex = 180
        '
        'txtExtras
        '
        Me.txtExtras.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource1, "EXTRAS", True))
        Me.txtExtras.Location = New System.Drawing.Point(218, 260)
        Me.txtExtras.Name = "txtExtras"
        Me.txtExtras.Size = New System.Drawing.Size(143, 20)
        Me.txtExtras.TabIndex = 156
        '
        'CCSSTextBox
        '
        Me.CCSSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource1, "CCSS", True))
        Me.CCSSTextBox.Location = New System.Drawing.Point(218, 72)
        Me.CCSSTextBox.Name = "CCSSTextBox"
        Me.CCSSTextBox.Size = New System.Drawing.Size(143, 20)
        Me.CCSSTextBox.TabIndex = 160
        '
        'SALARIO_DIARIOTextBox
        '
        Me.SALARIO_DIARIOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource1, "SALARIO DIARIO", True))
        Me.SALARIO_DIARIOTextBox.Location = New System.Drawing.Point(218, 45)
        Me.SALARIO_DIARIOTextBox.Name = "SALARIO_DIARIOTextBox"
        Me.SALARIO_DIARIOTextBox.Size = New System.Drawing.Size(143, 20)
        Me.SALARIO_DIARIOTextBox.TabIndex = 146
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(57, 105)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(105, 13)
        Me.Label3.TabIndex = 193
        Me.Label3.Text = "Año Impuesto Fiscal:"
        '
        'IMP_RENTATextBox
        '
        Me.IMP_RENTATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource1, "IMP RENTA", True))
        Me.IMP_RENTATextBox.Location = New System.Drawing.Point(216, 128)
        Me.IMP_RENTATextBox.Name = "IMP_RENTATextBox"
        Me.IMP_RENTATextBox.Size = New System.Drawing.Size(145, 20)
        Me.IMP_RENTATextBox.TabIndex = 194
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(SALARIO_UNICOLabel)
        Me.GroupBox4.Controls.Add(Me.SALARIO_UNICOTextBox1)
        Me.GroupBox4.Controls.Add(Me.comboxHorasExtrasDobles)
        Me.GroupBox4.Controls.Add(Me.horasExtrasRegulares)
        Me.GroupBox4.Controls.Add(Me.diazTrabajados)
        Me.GroupBox4.Controls.Add(IMP_RENTALabel)
        Me.GroupBox4.Controls.Add(Me.IMP_RENTATextBox)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.añoImpuestoFiscal)
        Me.GroupBox4.Controls.Add(Me.CESANTIATextBox)
        Me.GroupBox4.Controls.Add(CESANTIALabel)
        Me.GroupBox4.Controls.Add(SALARIO_DIARIOLabel)
        Me.GroupBox4.Controls.Add(Me.SALARIO_DIARIOTextBox)
        Me.GroupBox4.Controls.Add(Me.CCSSTextBox)
        Me.GroupBox4.Controls.Add(DIAS_TRABAJLabel)
        Me.GroupBox4.Controls.Add(CCSSLabel)
        Me.GroupBox4.Controls.Add(Me.txtExtras)
        Me.GroupBox4.Controls.Add(Label1)
        Me.GroupBox4.Controls.Add(HORAS_EXTRAS_REGULARLabel)
        Me.GroupBox4.Controls.Add(HORAS_EXTRAS_DOBLESLabel)
        Me.GroupBox4.Location = New System.Drawing.Point(444, 52)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(432, 295)
        Me.GroupBox4.TabIndex = 170
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Datos Laborales"
        '
        'SALARIO_UNICOTextBox1
        '
        Me.SALARIO_UNICOTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "SALARIO UNICO", True))
        Me.SALARIO_UNICOTextBox1.Location = New System.Drawing.Point(218, 20)
        Me.SALARIO_UNICOTextBox1.Name = "SALARIO_UNICOTextBox1"
        Me.SALARIO_UNICOTextBox1.Size = New System.Drawing.Size(143, 20)
        Me.SALARIO_UNICOTextBox1.TabIndex = 197
        '
        'PuestosBindingSource
        '
        Me.PuestosBindingSource.DataMember = "Puestos"
        Me.PuestosBindingSource.DataSource = Me.Planilla2DataSet1
        '
        'comboxHorasExtrasDobles
        '
        Me.comboxHorasExtrasDobles.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource1, "HORAS EXTRAS DOBLES", True))
        Me.comboxHorasExtrasDobles.FormattingEnabled = True
        Me.comboxHorasExtrasDobles.Items.AddRange(New Object() {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16"})
        Me.comboxHorasExtrasDobles.Location = New System.Drawing.Point(218, 235)
        Me.comboxHorasExtrasDobles.Name = "comboxHorasExtrasDobles"
        Me.comboxHorasExtrasDobles.Size = New System.Drawing.Size(143, 21)
        Me.comboxHorasExtrasDobles.TabIndex = 196
        '
        'horasExtrasRegulares
        '
        Me.horasExtrasRegulares.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource1, "HORAS EXTRAS REGULAR", True))
        Me.horasExtrasRegulares.FormattingEnabled = True
        Me.horasExtrasRegulares.Items.AddRange(New Object() {"0", "1", "2", "3", "4", "5", "6", "7", "8", ""})
        Me.horasExtrasRegulares.Location = New System.Drawing.Point(216, 207)
        Me.horasExtrasRegulares.Name = "horasExtrasRegulares"
        Me.horasExtrasRegulares.Size = New System.Drawing.Size(143, 21)
        Me.horasExtrasRegulares.TabIndex = 183
        '
        'diazTrabajados
        '
        Me.diazTrabajados.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource1, "DIAS TRABAJ", True))
        Me.diazTrabajados.FormattingEnabled = True
        Me.diazTrabajados.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"})
        Me.diazTrabajados.Location = New System.Drawing.Point(216, 152)
        Me.diazTrabajados.Name = "diazTrabajados"
        Me.diazTrabajados.Size = New System.Drawing.Size(143, 21)
        Me.diazTrabajados.TabIndex = 195
        '
        'añoImpuestoFiscal
        '
        Me.añoImpuestoFiscal.Location = New System.Drawing.Point(216, 101)
        Me.añoImpuestoFiscal.Name = "añoImpuestoFiscal"
        Me.añoImpuestoFiscal.Size = New System.Drawing.Size(145, 20)
        Me.añoImpuestoFiscal.TabIndex = 192
        '
        'CESANTIATextBox
        '
        Me.CESANTIATextBox.AcceptsReturn = True
        Me.CESANTIATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource1, "CESANTIA", True))
        Me.CESANTIATextBox.Location = New System.Drawing.Point(218, 177)
        Me.CESANTIATextBox.Name = "CESANTIATextBox"
        Me.CESANTIATextBox.Size = New System.Drawing.Size(143, 20)
        Me.CESANTIATextBox.TabIndex = 158
        '
        'DatosPersonalesTableAdapter
        '
        Me.DatosPersonalesTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager1
        '
        Me.TableAdapterManager1.AguinaldoTableAdapter = Nothing
        Me.TableAdapterManager1.AjustesTableAdapter = Me.AjustesTableAdapter
        Me.TableAdapterManager1.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager1.DatosPersonalesTableAdapter = Me.DatosPersonalesTableAdapter
        Me.TableAdapterManager1.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager1.ImpuestosTableAdapter = Nothing
        Me.TableAdapterManager1.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager1.PlanillaMensualTableAdapter = Me.PlanillaMensualTableAdapter1
        Me.TableAdapterManager1.PuestosTableAdapter = Nothing
        Me.TableAdapterManager1.UpdateOrder = PlanillasProyecto3.Planilla2DataSet1TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager1.UsuarioRegistroTableAdapter = Nothing
        Me.TableAdapterManager1.VacacionesTableAdapter = Nothing
        '
        'AjustesTableAdapter
        '
        Me.AjustesTableAdapter.ClearBeforeFill = True
        '
        'PlanillaMensualTableAdapter1
        '
        Me.PlanillaMensualTableAdapter1.ClearBeforeFill = True
        '
        'PuestosTableAdapter
        '
        Me.PuestosTableAdapter.ClearBeforeFill = True
        '
        'DeduccionesTableAdapter
        '
        Me.DeduccionesTableAdapter.ClearBeforeFill = True
        '
        'ImpuestosTableAdapter
        '
        Me.ImpuestosTableAdapter.ClearBeforeFill = True
        '
        'PlanillaMensualDataGridView
        '
        Me.PlanillaMensualDataGridView.AutoGenerateColumns = False
        Me.PlanillaMensualDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.PlanillaMensualDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.DataGridViewTextBoxColumn17, Me.DataGridViewTextBoxColumn18, Me.DataGridViewTextBoxColumn19, Me.DataGridViewTextBoxColumn20, Me.DataGridViewTextBoxColumn21, Me.DataGridViewTextBoxColumn22, Me.DataGridViewTextBoxColumn23})
        Me.PlanillaMensualDataGridView.DataSource = Me.PlanillaMensualBindingSource1
        Me.PlanillaMensualDataGridView.Location = New System.Drawing.Point(946, 592)
        Me.PlanillaMensualDataGridView.Name = "PlanillaMensualDataGridView"
        Me.PlanillaMensualDataGridView.Size = New System.Drawing.Size(300, 59)
        Me.PlanillaMensualDataGridView.TabIndex = 180
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "FechaPlanMensual"
        Me.DataGridViewTextBoxColumn1.HeaderText = "FechaPlanMensual"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "NoCEDULA"
        Me.DataGridViewTextBoxColumn2.HeaderText = "NoCEDULA"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Nombre"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Nombre"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "% ANUAL"
        Me.DataGridViewTextBoxColumn4.HeaderText = "% ANUAL"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "ANUALIDAD"
        Me.DataGridViewTextBoxColumn5.HeaderText = "ANUALIDAD"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "SALARIO BRUTO"
        Me.DataGridViewTextBoxColumn6.HeaderText = "SALARIO BRUTO"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "SALARIO DIARIO"
        Me.DataGridViewTextBoxColumn7.HeaderText = "SALARIO DIARIO"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "LIQUIDO A PAGAR"
        Me.DataGridViewTextBoxColumn8.HeaderText = "LIQUIDO A PAGAR"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "DIAS TRABAJ"
        Me.DataGridViewTextBoxColumn9.HeaderText = "DIAS TRABAJ"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "DiasIncap"
        Me.DataGridViewTextBoxColumn10.HeaderText = "DiasIncap"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "InstitucionIncapacidad"
        Me.DataGridViewTextBoxColumn11.HeaderText = "InstitucionIncapacidad"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "HORAS EXTRAS REGULAR"
        Me.DataGridViewTextBoxColumn12.HeaderText = "HORAS EXTRAS REGULAR"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "HORAS EXTRAS DOBLES"
        Me.DataGridViewTextBoxColumn13.HeaderText = "HORAS EXTRAS DOBLES"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "TOTAL DIAS INCAP"
        Me.DataGridViewTextBoxColumn14.HeaderText = "TOTAL DIAS INCAP"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.DataPropertyName = "FechaINCAPINI"
        Me.DataGridViewTextBoxColumn15.HeaderText = "FechaINCAPINI"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.DataPropertyName = "FechaINCAPFIN"
        Me.DataGridViewTextBoxColumn16.HeaderText = "FechaINCAPFIN"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.DataPropertyName = "Asiento"
        Me.DataGridViewTextBoxColumn17.HeaderText = "Asiento"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.DataPropertyName = "IMP RENTA"
        Me.DataGridViewTextBoxColumn18.HeaderText = "IMP RENTA"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.DataPropertyName = "EXTRAS"
        Me.DataGridViewTextBoxColumn19.HeaderText = "EXTRAS"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.DataPropertyName = "CESANTIA"
        Me.DataGridViewTextBoxColumn20.HeaderText = "CESANTIA"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.DataPropertyName = "SUBSIDIO"
        Me.DataGridViewTextBoxColumn21.HeaderText = "SUBSIDIO"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.DataPropertyName = "codDed"
        Me.DataGridViewTextBoxColumn22.HeaderText = "codDed"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.DataPropertyName = "CCSS"
        Me.DataGridViewTextBoxColumn23.HeaderText = "CCSS"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        '
        'Button2
        '
        Me.Button2.Image = Global.PlanillasProyecto3.My.Resources.Resources.iconoBuscarAutor
        Me.Button2.Location = New System.Drawing.Point(649, 539)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(66, 47)
        Me.Button2.TabIndex = 182
        Me.Button2.UseVisualStyleBackColor = True
        '
        'AsientoTextBox
        '
        Me.AsientoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource1, "Asiento", True))
        Me.AsientoTextBox.Location = New System.Drawing.Point(173, 106)
        Me.AsientoTextBox.Name = "AsientoTextBox"
        Me.AsientoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.AsientoTextBox.TabIndex = 138
        '
        'ANUALIDADTextBox
        '
        Me.ANUALIDADTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource1, "ANUALIDAD", True))
        Me.ANUALIDADTextBox.Location = New System.Drawing.Point(173, 79)
        Me.ANUALIDADTextBox.Name = "ANUALIDADTextBox"
        Me.ANUALIDADTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ANUALIDADTextBox.TabIndex = 114
        '
        '__ANUALTextBox
        '
        Me.__ANUALTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource1, "% ANUAL", True))
        Me.__ANUALTextBox.Location = New System.Drawing.Point(173, 53)
        Me.__ANUALTextBox.Name = "__ANUALTextBox"
        Me.__ANUALTextBox.Size = New System.Drawing.Size(100, 20)
        Me.__ANUALTextBox.TabIndex = 112
        '
        'SumaTotalTextBox
        '
        Me.SumaTotalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource1, "codDed", True))
        Me.SumaTotalTextBox.Location = New System.Drawing.Point(173, 161)
        Me.SumaTotalTextBox.Name = "SumaTotalTextBox"
        Me.SumaTotalTextBox.Size = New System.Drawing.Size(100, 20)
        Me.SumaTotalTextBox.TabIndex = 150
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(173, 135)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(100, 20)
        Me.TextBox2.TabIndex = 151
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(62, 138)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 13)
        Me.Label2.TabIndex = 152
        Me.Label2.Text = "Año Deducción:"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.FechaPlanMensualDateTimePicker)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.TextBox2)
        Me.GroupBox3.Controls.Add(SumaTotalLabel)
        Me.GroupBox3.Controls.Add(Me.SumaTotalTextBox)
        Me.GroupBox3.Controls.Add(FechaPlanMensualLabel)
        Me.GroupBox3.Controls.Add(Me.__ANUALTextBox)
        Me.GroupBox3.Controls.Add(__ANUALLabel)
        Me.GroupBox3.Controls.Add(ANUALIDADLabel)
        Me.GroupBox3.Controls.Add(Me.ANUALIDADTextBox)
        Me.GroupBox3.Controls.Add(Me.AsientoTextBox)
        Me.GroupBox3.Controls.Add(AsientoLabel)
        Me.GroupBox3.Location = New System.Drawing.Point(20, 378)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(412, 208)
        Me.GroupBox3.TabIndex = 171
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Otros Rubros"
        '
        'FechaPlanMensualDateTimePicker
        '
        Me.FechaPlanMensualDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.PlanillaMensualBindingSource1, "FechaPlanMensual", True))
        Me.FechaPlanMensualDateTimePicker.Location = New System.Drawing.Point(173, 27)
        Me.FechaPlanMensualDateTimePicker.Name = "FechaPlanMensualDateTimePicker"
        Me.FechaPlanMensualDateTimePicker.Size = New System.Drawing.Size(100, 20)
        Me.FechaPlanMensualDateTimePicker.TabIndex = 153
        '
        'PlanillaMensualDataGridView1
        '
        Me.PlanillaMensualDataGridView1.AutoGenerateColumns = False
        Me.PlanillaMensualDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.PlanillaMensualDataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn24, Me.DataGridViewTextBoxColumn25, Me.DataGridViewTextBoxColumn26, Me.DataGridViewTextBoxColumn27, Me.DataGridViewTextBoxColumn28, Me.DataGridViewTextBoxColumn29, Me.DataGridViewTextBoxColumn30, Me.DataGridViewTextBoxColumn31, Me.DataGridViewTextBoxColumn32, Me.DataGridViewTextBoxColumn33, Me.DataGridViewTextBoxColumn34, Me.DataGridViewTextBoxColumn35, Me.DataGridViewTextBoxColumn36, Me.DataGridViewTextBoxColumn37, Me.DataGridViewTextBoxColumn38, Me.DataGridViewTextBoxColumn39, Me.DataGridViewTextBoxColumn40, Me.DataGridViewTextBoxColumn41, Me.DataGridViewTextBoxColumn42, Me.DataGridViewTextBoxColumn43, Me.DataGridViewTextBoxColumn44, Me.DataGridViewTextBoxColumn45, Me.DataGridViewTextBoxColumn46})
        Me.PlanillaMensualDataGridView1.DataSource = Me.PlanillaMensualBindingSource1
        Me.PlanillaMensualDataGridView1.Location = New System.Drawing.Point(969, 266)
        Me.PlanillaMensualDataGridView1.Name = "PlanillaMensualDataGridView1"
        Me.PlanillaMensualDataGridView1.Size = New System.Drawing.Size(300, 220)
        Me.PlanillaMensualDataGridView1.TabIndex = 182
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.DataPropertyName = "FechaPlanMensual"
        Me.DataGridViewTextBoxColumn24.HeaderText = "FechaPlanMensual"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.DataPropertyName = "NoCEDULA"
        Me.DataGridViewTextBoxColumn25.HeaderText = "NoCEDULA"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.DataPropertyName = "Nombre"
        Me.DataGridViewTextBoxColumn26.HeaderText = "Nombre"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.DataPropertyName = "% ANUAL"
        Me.DataGridViewTextBoxColumn27.HeaderText = "% ANUAL"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.DataPropertyName = "ANUALIDAD"
        Me.DataGridViewTextBoxColumn28.HeaderText = "ANUALIDAD"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.DataPropertyName = "SALARIO BRUTO"
        Me.DataGridViewTextBoxColumn29.HeaderText = "SALARIO BRUTO"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.DataPropertyName = "SALARIO DIARIO"
        Me.DataGridViewTextBoxColumn30.HeaderText = "SALARIO DIARIO"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.DataPropertyName = "LIQUIDO A PAGAR"
        Me.DataGridViewTextBoxColumn31.HeaderText = "LIQUIDO A PAGAR"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.DataPropertyName = "DIAS TRABAJ"
        Me.DataGridViewTextBoxColumn32.HeaderText = "DIAS TRABAJ"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.DataPropertyName = "DiasIncap"
        Me.DataGridViewTextBoxColumn33.HeaderText = "DiasIncap"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        '
        'DataGridViewTextBoxColumn34
        '
        Me.DataGridViewTextBoxColumn34.DataPropertyName = "InstitucionIncapacidad"
        Me.DataGridViewTextBoxColumn34.HeaderText = "InstitucionIncapacidad"
        Me.DataGridViewTextBoxColumn34.Name = "DataGridViewTextBoxColumn34"
        '
        'DataGridViewTextBoxColumn35
        '
        Me.DataGridViewTextBoxColumn35.DataPropertyName = "HORAS EXTRAS REGULAR"
        Me.DataGridViewTextBoxColumn35.HeaderText = "HORAS EXTRAS REGULAR"
        Me.DataGridViewTextBoxColumn35.Name = "DataGridViewTextBoxColumn35"
        '
        'DataGridViewTextBoxColumn36
        '
        Me.DataGridViewTextBoxColumn36.DataPropertyName = "HORAS EXTRAS DOBLES"
        Me.DataGridViewTextBoxColumn36.HeaderText = "HORAS EXTRAS DOBLES"
        Me.DataGridViewTextBoxColumn36.Name = "DataGridViewTextBoxColumn36"
        '
        'DataGridViewTextBoxColumn37
        '
        Me.DataGridViewTextBoxColumn37.DataPropertyName = "TOTAL DIAS INCAP"
        Me.DataGridViewTextBoxColumn37.HeaderText = "TOTAL DIAS INCAP"
        Me.DataGridViewTextBoxColumn37.Name = "DataGridViewTextBoxColumn37"
        '
        'DataGridViewTextBoxColumn38
        '
        Me.DataGridViewTextBoxColumn38.DataPropertyName = "FechaINCAPINI"
        Me.DataGridViewTextBoxColumn38.HeaderText = "FechaINCAPINI"
        Me.DataGridViewTextBoxColumn38.Name = "DataGridViewTextBoxColumn38"
        '
        'DataGridViewTextBoxColumn39
        '
        Me.DataGridViewTextBoxColumn39.DataPropertyName = "FechaINCAPFIN"
        Me.DataGridViewTextBoxColumn39.HeaderText = "FechaINCAPFIN"
        Me.DataGridViewTextBoxColumn39.Name = "DataGridViewTextBoxColumn39"
        '
        'DataGridViewTextBoxColumn40
        '
        Me.DataGridViewTextBoxColumn40.DataPropertyName = "Asiento"
        Me.DataGridViewTextBoxColumn40.HeaderText = "Asiento"
        Me.DataGridViewTextBoxColumn40.Name = "DataGridViewTextBoxColumn40"
        '
        'DataGridViewTextBoxColumn41
        '
        Me.DataGridViewTextBoxColumn41.DataPropertyName = "IMP RENTA"
        Me.DataGridViewTextBoxColumn41.HeaderText = "IMP RENTA"
        Me.DataGridViewTextBoxColumn41.Name = "DataGridViewTextBoxColumn41"
        '
        'DataGridViewTextBoxColumn42
        '
        Me.DataGridViewTextBoxColumn42.DataPropertyName = "EXTRAS"
        Me.DataGridViewTextBoxColumn42.HeaderText = "EXTRAS"
        Me.DataGridViewTextBoxColumn42.Name = "DataGridViewTextBoxColumn42"
        '
        'DataGridViewTextBoxColumn43
        '
        Me.DataGridViewTextBoxColumn43.DataPropertyName = "CESANTIA"
        Me.DataGridViewTextBoxColumn43.HeaderText = "CESANTIA"
        Me.DataGridViewTextBoxColumn43.Name = "DataGridViewTextBoxColumn43"
        '
        'DataGridViewTextBoxColumn44
        '
        Me.DataGridViewTextBoxColumn44.DataPropertyName = "SUBSIDIO"
        Me.DataGridViewTextBoxColumn44.HeaderText = "SUBSIDIO"
        Me.DataGridViewTextBoxColumn44.Name = "DataGridViewTextBoxColumn44"
        '
        'DataGridViewTextBoxColumn45
        '
        Me.DataGridViewTextBoxColumn45.DataPropertyName = "codDed"
        Me.DataGridViewTextBoxColumn45.HeaderText = "codDed"
        Me.DataGridViewTextBoxColumn45.Name = "DataGridViewTextBoxColumn45"
        '
        'DataGridViewTextBoxColumn46
        '
        Me.DataGridViewTextBoxColumn46.DataPropertyName = "CCSS"
        Me.DataGridViewTextBoxColumn46.HeaderText = "CCSS"
        Me.DataGridViewTextBoxColumn46.Name = "DataGridViewTextBoxColumn46"
        '
        'frmPlanillaMensual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1306, 706)
        Me.Controls.Add(Me.PlanillaMensualDataGridView1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.PlanillaMensualDataGridView)
        Me.Controls.Add(CodImpuestoLabel)
        Me.Controls.Add(Me.CodImpuestoTextBox)
        Me.Controls.Add(ProcentajeImpuesto1Label)
        Me.Controls.Add(Me.ProcentajeImpuesto1TextBox)
        Me.Controls.Add(ProcentajeImpuesto2Label)
        Me.Controls.Add(Me.ProcentajeImpuesto2TextBox)
        Me.Controls.Add(CreditosFiscalesHijoLabel)
        Me.Controls.Add(Me.CreditosFiscalesHijoTextBox)
        Me.Controls.Add(CreditosFiscalesConyugeLabel)
        Me.Controls.Add(Me.CreditosFiscalesConyugeTextBox)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnRegresar)
        Me.Controls.Add(Me.LIQUIDO_A_PAGARTextBox)
        Me.Controls.Add(LIQUIDO_A_PAGARLabel)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.StatusStrip3)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "frmPlanillaMensual"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Planilla Mensual"
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PlanillaMensualBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.StatusStrip3.ResumeLayout(False)
        Me.StatusStrip3.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.PlanillaBisemanalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PlanillaMensualBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatosPersonalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Planilla2DataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AjustesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.DeduccionesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImpuestosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.PuestosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PlanillaMensualDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.PlanillaMensualDataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Planilla2DataSet As PlanillasProyecto3.Planilla2DataSet
    Friend WithEvents PlanillaMensualBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PlanillaMensualTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.PlanillaMensualTableAdapter
    Friend WithEvents TableAdapterManager As PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents StatusStrip3 As System.Windows.Forms.StatusStrip
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ArchivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegresarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresaerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BuscarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ModificarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NuevoRegistroToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents PlanillaBisemanalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PlanillaBisemanalTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.PlanillaBisemanalTableAdapter
    Friend WithEvents LIQUIDO_A_PAGARTextBox As System.Windows.Forms.TextBox
    Friend WithEvents btnRegresar As System.Windows.Forms.Button
    Friend WithEvents Planilla2DataSet1 As PlanillasProyecto3.Planilla2DataSet1
    Friend WithEvents DatosPersonalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DatosPersonalesTableAdapter As PlanillasProyecto3.Planilla2DataSet1TableAdapters.DatosPersonalesTableAdapter
    Friend WithEvents TableAdapterManager1 As PlanillasProyecto3.Planilla2DataSet1TableAdapters.TableAdapterManager
    Friend WithEvents AjustesTableAdapter As PlanillasProyecto3.Planilla2DataSet1TableAdapters.AjustesTableAdapter
    Friend WithEvents AjustesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PlanillaMensualTableAdapter1 As PlanillasProyecto3.Planilla2DataSet1TableAdapters.PlanillaMensualTableAdapter
    Friend WithEvents PlanillaMensualBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents PuestosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PuestosTableAdapter As PlanillasProyecto3.Planilla2DataSet1TableAdapters.PuestosTableAdapter
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents btnMostar As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TOTALAJUSTARTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NoCEDULATextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRE_COMPLETOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents E_MAILTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CODPUESTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents N__CONYUGESCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents N__HIJOSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents SUBSIDIOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TOTAL_DIAS_INCAPTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DeduccionesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DeduccionesTableAdapter As PlanillasProyecto3.Planilla2DataSet1TableAdapters.DeduccionesTableAdapter
    Friend WithEvents ImpuestosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ImpuestosTableAdapter As PlanillasProyecto3.Planilla2DataSet1TableAdapters.ImpuestosTableAdapter
    Friend WithEvents CodImpuestoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ProcentajeImpuesto1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents ProcentajeImpuesto2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents CreditosFiscalesHijoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CreditosFiscalesConyugeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents txtExtras As System.Windows.Forms.TextBox
    Friend WithEvents CCSSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALARIO_DIARIOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents IMP_RENTATextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents diazTrabajados As System.Windows.Forms.ComboBox
    Friend WithEvents horasExtrasRegulares As System.Windows.Forms.ComboBox
    Friend WithEvents comboxHorasExtrasDobles As System.Windows.Forms.ComboBox
    Friend WithEvents ComboInstitucion As System.Windows.Forms.ComboBox
    Friend WithEvents SALARIO_UNICOTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents añoImpuestoFiscal As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents PlanillaMensualDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CESANTIATextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConsultaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents AsientoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ANUALIDADTextBox As System.Windows.Forms.TextBox
    Friend WithEvents __ANUALTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SumaTotalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents FechaINCAPFINDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents FechaINCAPINIDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents FECHA_INGRESODateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents FechaPlanMensualDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents PlanillaMensualDataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn27 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn28 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn30 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn33 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn34 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn35 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn36 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn37 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn38 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn39 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn40 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn41 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn42 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn43 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn44 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn45 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn46 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
