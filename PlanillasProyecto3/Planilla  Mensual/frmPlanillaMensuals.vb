﻿Public Class frmPlanillaMensuals

    Private Sub DatosPersonalesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.DatosPersonalesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet1)

    End Sub

    Private Sub frmPlanillaMensuals_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet1.PlanillaMensual' table. You can move, or remove it, as needed.
        Me.PlanillaMensualTableAdapter.Fill(Me.Planilla2DataSet1.PlanillaMensual)
        'TODO: This line of code loads data into the 'Planilla2DataSet1.Ajustes' table. You can move, or remove it, as needed.
        Me.AjustesTableAdapter.Fill(Me.Planilla2DataSet1.Ajustes)
        'TODO: This line of code loads data into the 'Planilla2DataSet1.Puestos' table. You can move, or remove it, as needed.
        Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet1.Puestos)
        'TODO: This line of code loads data into the 'Planilla2DataSet1.Impuestos' table. You can move, or remove it, as needed.
        Me.ImpuestosTableAdapter.Fill(Me.Planilla2DataSet1.Impuestos)
        'TODO: This line of code loads data into the 'Planilla2DataSet1.Deducciones' table. You can move, or remove it, as needed.
        Me.DeduccionesTableAdapter.Fill(Me.Planilla2DataSet1.Deducciones)
        'TODO: This line of code loads data into the 'Planilla2DataSet1.DatosPersonales' table. You can move, or remove it, as needed.
        Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet1.DatosPersonales)

    End Sub

   
    
End Class