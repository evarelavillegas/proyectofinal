﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPlanillaMensuals
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ProcentajeImpuesto1Label As System.Windows.Forms.Label
        Dim ProcentajeImpuesto2Label As System.Windows.Forms.Label
        Dim CreditosFiscalesHijoLabel As System.Windows.Forms.Label
        Dim CreditosFiscalesConyugeLabel As System.Windows.Forms.Label
        Dim CodCedulaAjusteLabel As System.Windows.Forms.Label
        Dim TOTALAJUSTARLabel As System.Windows.Forms.Label
        Dim CODIGO_DE_PUESTOLabel As System.Windows.Forms.Label
        Dim SALARIO_UNICOLabel As System.Windows.Forms.Label
        Dim CodImpuestoLabel As System.Windows.Forms.Label
        Dim CodDeduccionLabel As System.Windows.Forms.Label
        Dim SumaTotalLabel As System.Windows.Forms.Label
        Dim NoCEDULALabel As System.Windows.Forms.Label
        Dim NOMBRE_COMPLETOLabel As System.Windows.Forms.Label
        Dim CODPUESTOLabel As System.Windows.Forms.Label
        Dim FECHA_INGRESOLabel As System.Windows.Forms.Label
        Dim AÑOSSERVLabel As System.Windows.Forms.Label
        Dim N__CONYUGESLabel As System.Windows.Forms.Label
        Dim N__HIJOSLabel As System.Windows.Forms.Label
        Dim FechaPlanMensualLabel As System.Windows.Forms.Label
        Dim __ANUALLabel As System.Windows.Forms.Label
        Dim ANUALIDADLabel As System.Windows.Forms.Label
        Dim SALARIO_DIARIOLabel As System.Windows.Forms.Label
        Dim LIQUIDO_A_PAGARLabel As System.Windows.Forms.Label
        Dim DIAS_TRABAJLabel As System.Windows.Forms.Label
        Dim InstitucionIncapacidadLabel As System.Windows.Forms.Label
        Dim HORAS_EXTRAS_REGULARLabel As System.Windows.Forms.Label
        Dim HORAS_EXTRAS_DOBLESLabel As System.Windows.Forms.Label
        Dim AsientoLabel As System.Windows.Forms.Label
        Dim IMP_RENTALabel As System.Windows.Forms.Label
        Dim EXTRASLabel As System.Windows.Forms.Label
        Dim CESANTIALabel As System.Windows.Forms.Label
        Dim SUBSIDIOLabel As System.Windows.Forms.Label
        Dim CodDedLabel As System.Windows.Forms.Label
        Dim CCSSLabel As System.Windows.Forms.Label
        Dim TOTAL_DIAS_INCAPLabel As System.Windows.Forms.Label
        Dim FechaINCAPINILabel As System.Windows.Forms.Label
        Dim FechaINCAPFINLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Me.Planilla2DataSet1 = New PlanillasProyecto3.Planilla2DataSet1()
        Me.DatosPersonalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DatosPersonalesTableAdapter = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.DatosPersonalesTableAdapter()
        Me.TableAdapterManager = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.TableAdapterManager()
        Me.DeduccionesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DeduccionesTableAdapter = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.DeduccionesTableAdapter()
        Me.ImpuestosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ImpuestosTableAdapter = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.ImpuestosTableAdapter()
        Me.ProcentajeImpuesto1TextBox = New System.Windows.Forms.TextBox()
        Me.ProcentajeImpuesto2TextBox = New System.Windows.Forms.TextBox()
        Me.CreditosFiscalesHijoTextBox = New System.Windows.Forms.TextBox()
        Me.CreditosFiscalesConyugeTextBox = New System.Windows.Forms.TextBox()
        Me.PuestosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PuestosTableAdapter = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.PuestosTableAdapter()
        Me.AjustesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AjustesTableAdapter = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.AjustesTableAdapter()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegresarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CodCedulaAjusteTextBox = New System.Windows.Forms.TextBox()
        Me.TOTALAJUSTARTextBox = New System.Windows.Forms.TextBox()
        Me.CODIGO_DE_PUESTOTextBox = New System.Windows.Forms.TextBox()
        Me.SALARIO_UNICOTextBox = New System.Windows.Forms.TextBox()
        Me.CodImpuestoTextBox = New System.Windows.Forms.TextBox()
        Me.CodDeduccionTextBox = New System.Windows.Forms.TextBox()
        Me.SumaTotalTextBox = New System.Windows.Forms.TextBox()
        Me.NoCEDULATextBox = New System.Windows.Forms.TextBox()
        Me.NOMBRE_COMPLETOTextBox = New System.Windows.Forms.TextBox()
        Me.CODPUESTOTextBox = New System.Windows.Forms.TextBox()
        Me.FECHA_INGRESODateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.AÑOSSERVTextBox = New System.Windows.Forms.TextBox()
        Me.N__CONYUGESCheckBox = New System.Windows.Forms.CheckBox()
        Me.N__HIJOSTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.FechaPlanMensualDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.PlanillaMensualBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.__ANUALTextBox = New System.Windows.Forms.TextBox()
        Me.ANUALIDADTextBox = New System.Windows.Forms.TextBox()
        Me.SALARIO_DIARIOTextBox = New System.Windows.Forms.TextBox()
        Me.AsientoTextBox = New System.Windows.Forms.TextBox()
        Me.IMP_RENTATextBox = New System.Windows.Forms.TextBox()
        Me.CESANTIATextBox = New System.Windows.Forms.TextBox()
        Me.CodDedTextBox = New System.Windows.Forms.TextBox()
        Me.CCSSTextBox = New System.Windows.Forms.TextBox()
        Me.PlanillaMensualTableAdapter = New PlanillasProyecto3.Planilla2DataSet1TableAdapters.PlanillaMensualTableAdapter()
        Me.LIQUIDO_A_PAGARTextBox = New System.Windows.Forms.TextBox()
        Me.DIAS_TRABAJTextBox = New System.Windows.Forms.TextBox()
        Me.HORAS_EXTRAS_REGULARTextBox = New System.Windows.Forms.TextBox()
        Me.HORAS_EXTRAS_DOBLESTextBox = New System.Windows.Forms.TextBox()
        Me.EXTRASTextBox = New System.Windows.Forms.TextBox()
        Me.SUBSIDIOTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.InstitucionIncapacidadTextBox = New System.Windows.Forms.ComboBox()
        Me.TOTAL_DIAS_INCAPTextBox = New System.Windows.Forms.TextBox()
        Me.FechaINCAPINIDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.FechaINCAPFINDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        ProcentajeImpuesto1Label = New System.Windows.Forms.Label()
        ProcentajeImpuesto2Label = New System.Windows.Forms.Label()
        CreditosFiscalesHijoLabel = New System.Windows.Forms.Label()
        CreditosFiscalesConyugeLabel = New System.Windows.Forms.Label()
        CodCedulaAjusteLabel = New System.Windows.Forms.Label()
        TOTALAJUSTARLabel = New System.Windows.Forms.Label()
        CODIGO_DE_PUESTOLabel = New System.Windows.Forms.Label()
        SALARIO_UNICOLabel = New System.Windows.Forms.Label()
        CodImpuestoLabel = New System.Windows.Forms.Label()
        CodDeduccionLabel = New System.Windows.Forms.Label()
        SumaTotalLabel = New System.Windows.Forms.Label()
        NoCEDULALabel = New System.Windows.Forms.Label()
        NOMBRE_COMPLETOLabel = New System.Windows.Forms.Label()
        CODPUESTOLabel = New System.Windows.Forms.Label()
        FECHA_INGRESOLabel = New System.Windows.Forms.Label()
        AÑOSSERVLabel = New System.Windows.Forms.Label()
        N__CONYUGESLabel = New System.Windows.Forms.Label()
        N__HIJOSLabel = New System.Windows.Forms.Label()
        FechaPlanMensualLabel = New System.Windows.Forms.Label()
        __ANUALLabel = New System.Windows.Forms.Label()
        ANUALIDADLabel = New System.Windows.Forms.Label()
        SALARIO_DIARIOLabel = New System.Windows.Forms.Label()
        LIQUIDO_A_PAGARLabel = New System.Windows.Forms.Label()
        DIAS_TRABAJLabel = New System.Windows.Forms.Label()
        InstitucionIncapacidadLabel = New System.Windows.Forms.Label()
        HORAS_EXTRAS_REGULARLabel = New System.Windows.Forms.Label()
        HORAS_EXTRAS_DOBLESLabel = New System.Windows.Forms.Label()
        AsientoLabel = New System.Windows.Forms.Label()
        IMP_RENTALabel = New System.Windows.Forms.Label()
        EXTRASLabel = New System.Windows.Forms.Label()
        CESANTIALabel = New System.Windows.Forms.Label()
        SUBSIDIOLabel = New System.Windows.Forms.Label()
        CodDedLabel = New System.Windows.Forms.Label()
        CCSSLabel = New System.Windows.Forms.Label()
        TOTAL_DIAS_INCAPLabel = New System.Windows.Forms.Label()
        FechaINCAPINILabel = New System.Windows.Forms.Label()
        FechaINCAPFINLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        CType(Me.Planilla2DataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatosPersonalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DeduccionesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImpuestosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PuestosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AjustesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.PlanillaMensualBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'ProcentajeImpuesto1Label
        '
        ProcentajeImpuesto1Label.AutoSize = True
        ProcentajeImpuesto1Label.Location = New System.Drawing.Point(-30, -22)
        ProcentajeImpuesto1Label.Name = "ProcentajeImpuesto1Label"
        ProcentajeImpuesto1Label.Size = New System.Drawing.Size(112, 13)
        ProcentajeImpuesto1Label.TabIndex = 63
        ProcentajeImpuesto1Label.Text = "procentaje Impuesto1:"
        ProcentajeImpuesto1Label.Visible = False
        '
        'ProcentajeImpuesto2Label
        '
        ProcentajeImpuesto2Label.AutoSize = True
        ProcentajeImpuesto2Label.Location = New System.Drawing.Point(126, -50)
        ProcentajeImpuesto2Label.Name = "ProcentajeImpuesto2Label"
        ProcentajeImpuesto2Label.Size = New System.Drawing.Size(112, 13)
        ProcentajeImpuesto2Label.TabIndex = 65
        ProcentajeImpuesto2Label.Text = "procentaje Impuesto2:"
        ProcentajeImpuesto2Label.Visible = False
        '
        'CreditosFiscalesHijoLabel
        '
        CreditosFiscalesHijoLabel.AutoSize = True
        CreditosFiscalesHijoLabel.Location = New System.Drawing.Point(192, 35)
        CreditosFiscalesHijoLabel.Name = "CreditosFiscalesHijoLabel"
        CreditosFiscalesHijoLabel.Size = New System.Drawing.Size(109, 13)
        CreditosFiscalesHijoLabel.TabIndex = 67
        CreditosFiscalesHijoLabel.Text = "creditos Fiscales Hijo:"
        CreditosFiscalesHijoLabel.Visible = False
        '
        'CreditosFiscalesConyugeLabel
        '
        CreditosFiscalesConyugeLabel.AutoSize = True
        CreditosFiscalesConyugeLabel.Location = New System.Drawing.Point(101, 32)
        CreditosFiscalesConyugeLabel.Name = "CreditosFiscalesConyugeLabel"
        CreditosFiscalesConyugeLabel.Size = New System.Drawing.Size(133, 13)
        CreditosFiscalesConyugeLabel.TabIndex = 69
        CreditosFiscalesConyugeLabel.Text = "creditos Fiscales Conyuge:"
        CreditosFiscalesConyugeLabel.Visible = False
        '
        'CodCedulaAjusteLabel
        '
        CodCedulaAjusteLabel.AutoSize = True
        CodCedulaAjusteLabel.Location = New System.Drawing.Point(11, 316)
        CodCedulaAjusteLabel.Name = "CodCedulaAjusteLabel"
        CodCedulaAjusteLabel.Size = New System.Drawing.Size(97, 13)
        CodCedulaAjusteLabel.TabIndex = 127
        CodCedulaAjusteLabel.Text = "Cod Cedula Ajuste:"
        '
        'TOTALAJUSTARLabel
        '
        TOTALAJUSTARLabel.AutoSize = True
        TOTALAJUSTARLabel.Location = New System.Drawing.Point(14, 339)
        TOTALAJUSTARLabel.Name = "TOTALAJUSTARLabel"
        TOTALAJUSTARLabel.Size = New System.Drawing.Size(94, 13)
        TOTALAJUSTARLabel.TabIndex = 129
        TOTALAJUSTARLabel.Text = "TOTALAJUSTAR:"
        '
        'CODIGO_DE_PUESTOLabel
        '
        CODIGO_DE_PUESTOLabel.AutoSize = True
        CODIGO_DE_PUESTOLabel.Location = New System.Drawing.Point(9, 269)
        CODIGO_DE_PUESTOLabel.Name = "CODIGO_DE_PUESTOLabel"
        CODIGO_DE_PUESTOLabel.Size = New System.Drawing.Size(117, 13)
        CODIGO_DE_PUESTOLabel.TabIndex = 123
        CODIGO_DE_PUESTOLabel.Text = "CODIGO DE PUESTO:"
        '
        'SALARIO_UNICOLabel
        '
        SALARIO_UNICOLabel.AutoSize = True
        SALARIO_UNICOLabel.Location = New System.Drawing.Point(11, 293)
        SALARIO_UNICOLabel.Name = "SALARIO_UNICOLabel"
        SALARIO_UNICOLabel.Size = New System.Drawing.Size(93, 13)
        SALARIO_UNICOLabel.TabIndex = 125
        SALARIO_UNICOLabel.Text = "SALARIO UNICO:"
        '
        'CodImpuestoLabel
        '
        CodImpuestoLabel.AutoSize = True
        CodImpuestoLabel.Location = New System.Drawing.Point(15, 368)
        CodImpuestoLabel.Name = "CodImpuestoLabel"
        CodImpuestoLabel.Size = New System.Drawing.Size(74, 13)
        CodImpuestoLabel.TabIndex = 121
        CodImpuestoLabel.Text = "cod Impuesto:"
        '
        'CodDeduccionLabel
        '
        CodDeduccionLabel.AutoSize = True
        CodDeduccionLabel.Location = New System.Drawing.Point(22, 216)
        CodDeduccionLabel.Name = "CodDeduccionLabel"
        CodDeduccionLabel.Size = New System.Drawing.Size(83, 13)
        CodDeduccionLabel.TabIndex = 117
        CodDeduccionLabel.Text = "cod Deduccion:"
        '
        'SumaTotalLabel
        '
        SumaTotalLabel.AutoSize = True
        SumaTotalLabel.Location = New System.Drawing.Point(22, 242)
        SumaTotalLabel.Name = "SumaTotalLabel"
        SumaTotalLabel.Size = New System.Drawing.Size(64, 13)
        SumaTotalLabel.TabIndex = 119
        SumaTotalLabel.Text = "Suma Total:"
        '
        'NoCEDULALabel
        '
        NoCEDULALabel.AutoSize = True
        NoCEDULALabel.Location = New System.Drawing.Point(20, 37)
        NoCEDULALabel.Name = "NoCEDULALabel"
        NoCEDULALabel.Size = New System.Drawing.Size(70, 13)
        NoCEDULALabel.TabIndex = 103
        NoCEDULALabel.Text = "No CEDULA:"
        '
        'NOMBRE_COMPLETOLabel
        '
        NOMBRE_COMPLETOLabel.AutoSize = True
        NOMBRE_COMPLETOLabel.Location = New System.Drawing.Point(20, 60)
        NOMBRE_COMPLETOLabel.Name = "NOMBRE_COMPLETOLabel"
        NOMBRE_COMPLETOLabel.Size = New System.Drawing.Size(119, 13)
        NOMBRE_COMPLETOLabel.TabIndex = 105
        NOMBRE_COMPLETOLabel.Text = "NOMBRE COMPLETO:"
        '
        'CODPUESTOLabel
        '
        CODPUESTOLabel.AutoSize = True
        CODPUESTOLabel.Location = New System.Drawing.Point(20, 86)
        CODPUESTOLabel.Name = "CODPUESTOLabel"
        CODPUESTOLabel.Size = New System.Drawing.Size(77, 13)
        CODPUESTOLabel.TabIndex = 107
        CODPUESTOLabel.Text = "CODPUESTO:"
        '
        'FECHA_INGRESOLabel
        '
        FECHA_INGRESOLabel.AutoSize = True
        FECHA_INGRESOLabel.Location = New System.Drawing.Point(20, 113)
        FECHA_INGRESOLabel.Name = "FECHA_INGRESOLabel"
        FECHA_INGRESOLabel.Size = New System.Drawing.Size(97, 13)
        FECHA_INGRESOLabel.TabIndex = 109
        FECHA_INGRESOLabel.Text = "FECHA INGRESO:"
        '
        'AÑOSSERVLabel
        '
        AÑOSSERVLabel.AutoSize = True
        AÑOSSERVLabel.Location = New System.Drawing.Point(20, 138)
        AÑOSSERVLabel.Name = "AÑOSSERVLabel"
        AÑOSSERVLabel.Size = New System.Drawing.Size(69, 13)
        AÑOSSERVLabel.TabIndex = 111
        AÑOSSERVLabel.Text = "AÑOSSERV:"
        '
        'N__CONYUGESLabel
        '
        N__CONYUGESLabel.AutoSize = True
        N__CONYUGESLabel.Location = New System.Drawing.Point(20, 166)
        N__CONYUGESLabel.Name = "N__CONYUGESLabel"
        N__CONYUGESLabel.Size = New System.Drawing.Size(85, 13)
        N__CONYUGESLabel.TabIndex = 113
        N__CONYUGESLabel.Text = "N° CONYUGES:"
        '
        'N__HIJOSLabel
        '
        N__HIJOSLabel.AutoSize = True
        N__HIJOSLabel.Location = New System.Drawing.Point(20, 190)
        N__HIJOSLabel.Name = "N__HIJOSLabel"
        N__HIJOSLabel.Size = New System.Drawing.Size(56, 13)
        N__HIJOSLabel.TabIndex = 115
        N__HIJOSLabel.Text = "N° HIJOS:"
        '
        'FechaPlanMensualLabel
        '
        FechaPlanMensualLabel.AutoSize = True
        FechaPlanMensualLabel.Location = New System.Drawing.Point(187, -27)
        FechaPlanMensualLabel.Name = "FechaPlanMensualLabel"
        FechaPlanMensualLabel.Size = New System.Drawing.Size(107, 13)
        FechaPlanMensualLabel.TabIndex = 0
        FechaPlanMensualLabel.Text = "Fecha Plan Mensual:"
        '
        '__ANUALLabel
        '
        __ANUALLabel.AutoSize = True
        __ANUALLabel.Location = New System.Drawing.Point(31, 56)
        __ANUALLabel.Name = "__ANUALLabel"
        __ANUALLabel.Size = New System.Drawing.Size(57, 13)
        __ANUALLabel.TabIndex = 6
        __ANUALLabel.Text = "% ANUAL:"
        '
        'ANUALIDADLabel
        '
        ANUALIDADLabel.AutoSize = True
        ANUALIDADLabel.Location = New System.Drawing.Point(31, 82)
        ANUALIDADLabel.Name = "ANUALIDADLabel"
        ANUALIDADLabel.Size = New System.Drawing.Size(72, 13)
        ANUALIDADLabel.TabIndex = 8
        ANUALIDADLabel.Text = "ANUALIDAD:"
        '
        'SALARIO_DIARIOLabel
        '
        SALARIO_DIARIOLabel.AutoSize = True
        SALARIO_DIARIOLabel.Location = New System.Drawing.Point(31, 108)
        SALARIO_DIARIOLabel.Name = "SALARIO_DIARIOLabel"
        SALARIO_DIARIOLabel.Size = New System.Drawing.Size(96, 13)
        SALARIO_DIARIOLabel.TabIndex = 12
        SALARIO_DIARIOLabel.Text = "SALARIO DIARIO:"
        '
        'LIQUIDO_A_PAGARLabel
        '
        LIQUIDO_A_PAGARLabel.AutoSize = True
        LIQUIDO_A_PAGARLabel.Location = New System.Drawing.Point(12, 147)
        LIQUIDO_A_PAGARLabel.Name = "LIQUIDO_A_PAGARLabel"
        LIQUIDO_A_PAGARLabel.Size = New System.Drawing.Size(104, 13)
        LIQUIDO_A_PAGARLabel.TabIndex = 14
        LIQUIDO_A_PAGARLabel.Text = "LIQUIDO A PAGAR:"
        '
        'DIAS_TRABAJLabel
        '
        DIAS_TRABAJLabel.AutoSize = True
        DIAS_TRABAJLabel.Location = New System.Drawing.Point(12, 43)
        DIAS_TRABAJLabel.Name = "DIAS_TRABAJLabel"
        DIAS_TRABAJLabel.Size = New System.Drawing.Size(79, 13)
        DIAS_TRABAJLabel.TabIndex = 16
        DIAS_TRABAJLabel.Text = "DIAS TRABAJ:"
        '
        'InstitucionIncapacidadLabel
        '
        InstitucionIncapacidadLabel.AutoSize = True
        InstitucionIncapacidadLabel.Location = New System.Drawing.Point(10, 30)
        InstitucionIncapacidadLabel.Name = "InstitucionIncapacidadLabel"
        InstitucionIncapacidadLabel.Size = New System.Drawing.Size(120, 13)
        InstitucionIncapacidadLabel.TabIndex = 20
        InstitucionIncapacidadLabel.Text = "Institucion Incapacidad:"
        '
        'HORAS_EXTRAS_REGULARLabel
        '
        HORAS_EXTRAS_REGULARLabel.AutoSize = True
        HORAS_EXTRAS_REGULARLabel.Location = New System.Drawing.Point(12, 69)
        HORAS_EXTRAS_REGULARLabel.Name = "HORAS_EXTRAS_REGULARLabel"
        HORAS_EXTRAS_REGULARLabel.Size = New System.Drawing.Size(149, 13)
        HORAS_EXTRAS_REGULARLabel.TabIndex = 22
        HORAS_EXTRAS_REGULARLabel.Text = "HORAS EXTRAS REGULAR:"
        '
        'HORAS_EXTRAS_DOBLESLabel
        '
        HORAS_EXTRAS_DOBLESLabel.AutoSize = True
        HORAS_EXTRAS_DOBLESLabel.Location = New System.Drawing.Point(12, 95)
        HORAS_EXTRAS_DOBLESLabel.Name = "HORAS_EXTRAS_DOBLESLabel"
        HORAS_EXTRAS_DOBLESLabel.Size = New System.Drawing.Size(140, 13)
        HORAS_EXTRAS_DOBLESLabel.TabIndex = 24
        HORAS_EXTRAS_DOBLESLabel.Text = "HORAS EXTRAS DOBLES:"
        '
        'AsientoLabel
        '
        AsientoLabel.AutoSize = True
        AsientoLabel.Location = New System.Drawing.Point(31, 234)
        AsientoLabel.Name = "AsientoLabel"
        AsientoLabel.Size = New System.Drawing.Size(45, 13)
        AsientoLabel.TabIndex = 32
        AsientoLabel.Text = "Asiento:"
        '
        'IMP_RENTALabel
        '
        IMP_RENTALabel.AutoSize = True
        IMP_RENTALabel.Location = New System.Drawing.Point(31, 134)
        IMP_RENTALabel.Name = "IMP_RENTALabel"
        IMP_RENTALabel.Size = New System.Drawing.Size(69, 13)
        IMP_RENTALabel.TabIndex = 34
        IMP_RENTALabel.Text = "IMP RENTA:"
        '
        'EXTRASLabel
        '
        EXTRASLabel.AutoSize = True
        EXTRASLabel.Location = New System.Drawing.Point(12, 121)
        EXTRASLabel.Name = "EXTRASLabel"
        EXTRASLabel.Size = New System.Drawing.Size(53, 13)
        EXTRASLabel.TabIndex = 36
        EXTRASLabel.Text = "EXTRAS:"
        '
        'CESANTIALabel
        '
        CESANTIALabel.AutoSize = True
        CESANTIALabel.Location = New System.Drawing.Point(31, 157)
        CESANTIALabel.Name = "CESANTIALabel"
        CESANTIALabel.Size = New System.Drawing.Size(63, 13)
        CESANTIALabel.TabIndex = 38
        CESANTIALabel.Text = "CESANTIA:"
        '
        'SUBSIDIOLabel
        '
        SUBSIDIOLabel.AutoSize = True
        SUBSIDIOLabel.Location = New System.Drawing.Point(4, 132)
        SUBSIDIOLabel.Name = "SUBSIDIOLabel"
        SUBSIDIOLabel.Size = New System.Drawing.Size(61, 13)
        SUBSIDIOLabel.TabIndex = 40
        SUBSIDIOLabel.Text = "SUBSIDIO:"
        '
        'CodDedLabel
        '
        CodDedLabel.AutoSize = True
        CodDedLabel.Location = New System.Drawing.Point(31, 182)
        CodDedLabel.Name = "CodDedLabel"
        CodDedLabel.Size = New System.Drawing.Size(51, 13)
        CodDedLabel.TabIndex = 42
        CodDedLabel.Text = "cod Ded:"
        '
        'CCSSLabel
        '
        CCSSLabel.AutoSize = True
        CCSSLabel.Location = New System.Drawing.Point(31, 208)
        CCSSLabel.Name = "CCSSLabel"
        CCSSLabel.Size = New System.Drawing.Size(38, 13)
        CCSSLabel.TabIndex = 44
        CCSSLabel.Text = "CCSS:"
        '
        'TOTAL_DIAS_INCAPLabel
        '
        TOTAL_DIAS_INCAPLabel.AutoSize = True
        TOTAL_DIAS_INCAPLabel.Location = New System.Drawing.Point(4, 110)
        TOTAL_DIAS_INCAPLabel.Name = "TOTAL_DIAS_INCAPLabel"
        TOTAL_DIAS_INCAPLabel.Size = New System.Drawing.Size(108, 13)
        TOTAL_DIAS_INCAPLabel.TabIndex = 32
        TOTAL_DIAS_INCAPLabel.Text = "TOTAL DIAS INCAP:"
        '
        'FechaINCAPINILabel
        '
        FechaINCAPINILabel.AutoSize = True
        FechaINCAPINILabel.Location = New System.Drawing.Point(6, 60)
        FechaINCAPINILabel.Name = "FechaINCAPINILabel"
        FechaINCAPINILabel.Size = New System.Drawing.Size(89, 13)
        FechaINCAPINILabel.TabIndex = 34
        FechaINCAPINILabel.Text = "Fecha INCAPINI:"
        '
        'FechaINCAPFINLabel
        '
        FechaINCAPFINLabel.AutoSize = True
        FechaINCAPFINLabel.Location = New System.Drawing.Point(6, 86)
        FechaINCAPFINLabel.Name = "FechaINCAPFINLabel"
        FechaINCAPFINLabel.Size = New System.Drawing.Size(92, 13)
        FechaINCAPFINLabel.TabIndex = 36
        FechaINCAPFINLabel.Text = "Fecha INCAPFIN:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(31, 29)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(73, 13)
        Label1.TabIndex = 65
        Label1.Text = "Fecha Planilla"
        '
        'Planilla2DataSet1
        '
        Me.Planilla2DataSet1.DataSetName = "Planilla2DataSet1"
        Me.Planilla2DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DatosPersonalesBindingSource
        '
        Me.DatosPersonalesBindingSource.DataMember = "DatosPersonales"
        Me.DatosPersonalesBindingSource.DataSource = Me.Planilla2DataSet1
        '
        'DatosPersonalesTableAdapter
        '
        Me.DatosPersonalesTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AguinaldoTableAdapter = Nothing
        Me.TableAdapterManager.AjustesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Me.DatosPersonalesTableAdapter
        Me.TableAdapterManager.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager.ImpuestosTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager.PuestosTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = PlanillasProyecto3.Planilla2DataSet1TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UsuarioRegistroTableAdapter = Nothing
        Me.TableAdapterManager.VacacionesTableAdapter = Nothing
        '
        'DeduccionesBindingSource
        '
        Me.DeduccionesBindingSource.DataMember = "Deducciones"
        Me.DeduccionesBindingSource.DataSource = Me.Planilla2DataSet1
        '
        'DeduccionesTableAdapter
        '
        Me.DeduccionesTableAdapter.ClearBeforeFill = True
        '
        'ImpuestosBindingSource
        '
        Me.ImpuestosBindingSource.DataMember = "Impuestos"
        Me.ImpuestosBindingSource.DataSource = Me.Planilla2DataSet1
        '
        'ImpuestosTableAdapter
        '
        Me.ImpuestosTableAdapter.ClearBeforeFill = True
        '
        'ProcentajeImpuesto1TextBox
        '
        Me.ProcentajeImpuesto1TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "procentajeImpuesto1", True))
        Me.ProcentajeImpuesto1TextBox.Location = New System.Drawing.Point(109, -25)
        Me.ProcentajeImpuesto1TextBox.Name = "ProcentajeImpuesto1TextBox"
        Me.ProcentajeImpuesto1TextBox.Size = New System.Drawing.Size(100, 20)
        Me.ProcentajeImpuesto1TextBox.TabIndex = 64
        Me.ProcentajeImpuesto1TextBox.Visible = False
        '
        'ProcentajeImpuesto2TextBox
        '
        Me.ProcentajeImpuesto2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "procentajeImpuesto2", True))
        Me.ProcentajeImpuesto2TextBox.Location = New System.Drawing.Point(265, -53)
        Me.ProcentajeImpuesto2TextBox.Name = "ProcentajeImpuesto2TextBox"
        Me.ProcentajeImpuesto2TextBox.Size = New System.Drawing.Size(100, 20)
        Me.ProcentajeImpuesto2TextBox.TabIndex = 66
        Me.ProcentajeImpuesto2TextBox.Visible = False
        '
        'CreditosFiscalesHijoTextBox
        '
        Me.CreditosFiscalesHijoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "creditosFiscalesHijo", True))
        Me.CreditosFiscalesHijoTextBox.Location = New System.Drawing.Point(265, -27)
        Me.CreditosFiscalesHijoTextBox.Name = "CreditosFiscalesHijoTextBox"
        Me.CreditosFiscalesHijoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CreditosFiscalesHijoTextBox.TabIndex = 68
        Me.CreditosFiscalesHijoTextBox.Visible = False
        '
        'CreditosFiscalesConyugeTextBox
        '
        Me.CreditosFiscalesConyugeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "creditosFiscalesConyuge", True))
        Me.CreditosFiscalesConyugeTextBox.Location = New System.Drawing.Point(240, 29)
        Me.CreditosFiscalesConyugeTextBox.Name = "CreditosFiscalesConyugeTextBox"
        Me.CreditosFiscalesConyugeTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CreditosFiscalesConyugeTextBox.TabIndex = 70
        Me.CreditosFiscalesConyugeTextBox.Visible = False
        '
        'PuestosBindingSource
        '
        Me.PuestosBindingSource.DataMember = "Puestos"
        Me.PuestosBindingSource.DataSource = Me.Planilla2DataSet1
        '
        'PuestosTableAdapter
        '
        Me.PuestosTableAdapter.ClearBeforeFill = True
        '
        'AjustesBindingSource
        '
        Me.AjustesBindingSource.DataMember = "Ajustes"
        Me.AjustesBindingSource.DataSource = Me.Planilla2DataSet1
        '
        'AjustesTableAdapter
        '
        Me.AjustesTableAdapter.ClearBeforeFill = True
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripButton2, Me.ToolStripButton3})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(785, 25)
        Me.ToolStrip1.TabIndex = 103
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = Global.PlanillasProyecto3.My.Resources.Resources.guardar_documento_icono_7840_48
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "ToolStripButton1"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.Image = Global.PlanillasProyecto3.My.Resources.Resources.iconoBuscarAutor
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton2.Text = "ToolStripButton2"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton3.Image = Global.PlanillasProyecto3.My.Resources.Resources.Eliminar
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton3.Text = "ToolStripButton3"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 652)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(785, 22)
        Me.StatusStrip1.TabIndex = 104
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(39, 17)
        Me.ToolStripStatusLabel1.Text = "Status"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivoToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(785, 24)
        Me.MenuStrip1.TabIndex = 105
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ArchivoToolStripMenuItem
        '
        Me.ArchivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegresarToolStripMenuItem})
        Me.ArchivoToolStripMenuItem.Name = "ArchivoToolStripMenuItem"
        Me.ArchivoToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.ArchivoToolStripMenuItem.Text = "&Archivo"
        '
        'RegresarToolStripMenuItem
        '
        Me.RegresarToolStripMenuItem.Name = "RegresarToolStripMenuItem"
        Me.RegresarToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.RegresarToolStripMenuItem.Text = "&Regresar"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(FechaPlanMensualLabel)
        Me.GroupBox1.Controls.Add(CodCedulaAjusteLabel)
        Me.GroupBox1.Controls.Add(Me.CodCedulaAjusteTextBox)
        Me.GroupBox1.Controls.Add(TOTALAJUSTARLabel)
        Me.GroupBox1.Controls.Add(Me.TOTALAJUSTARTextBox)
        Me.GroupBox1.Controls.Add(CODIGO_DE_PUESTOLabel)
        Me.GroupBox1.Controls.Add(Me.CODIGO_DE_PUESTOTextBox)
        Me.GroupBox1.Controls.Add(SALARIO_UNICOLabel)
        Me.GroupBox1.Controls.Add(Me.SALARIO_UNICOTextBox)
        Me.GroupBox1.Controls.Add(CodImpuestoLabel)
        Me.GroupBox1.Controls.Add(Me.CodImpuestoTextBox)
        Me.GroupBox1.Controls.Add(CodDeduccionLabel)
        Me.GroupBox1.Controls.Add(Me.CodDeduccionTextBox)
        Me.GroupBox1.Controls.Add(SumaTotalLabel)
        Me.GroupBox1.Controls.Add(Me.SumaTotalTextBox)
        Me.GroupBox1.Controls.Add(ProcentajeImpuesto2Label)
        Me.GroupBox1.Controls.Add(NoCEDULALabel)
        Me.GroupBox1.Controls.Add(Me.NoCEDULATextBox)
        Me.GroupBox1.Controls.Add(Me.ProcentajeImpuesto2TextBox)
        Me.GroupBox1.Controls.Add(NOMBRE_COMPLETOLabel)
        Me.GroupBox1.Controls.Add(Me.NOMBRE_COMPLETOTextBox)
        Me.GroupBox1.Controls.Add(CODPUESTOLabel)
        Me.GroupBox1.Controls.Add(Me.CODPUESTOTextBox)
        Me.GroupBox1.Controls.Add(Me.CreditosFiscalesHijoTextBox)
        Me.GroupBox1.Controls.Add(FECHA_INGRESOLabel)
        Me.GroupBox1.Controls.Add(Me.FECHA_INGRESODateTimePicker)
        Me.GroupBox1.Controls.Add(AÑOSSERVLabel)
        Me.GroupBox1.Controls.Add(Me.AÑOSSERVTextBox)
        Me.GroupBox1.Controls.Add(N__CONYUGESLabel)
        Me.GroupBox1.Controls.Add(Me.N__CONYUGESCheckBox)
        Me.GroupBox1.Controls.Add(N__HIJOSLabel)
        Me.GroupBox1.Controls.Add(Me.N__HIJOSTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 52)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(365, 395)
        Me.GroupBox1.TabIndex = 106
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "DatosEmpleado"
        '
        'CodCedulaAjusteTextBox
        '
        Me.CodCedulaAjusteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AjustesBindingSource, "CodCedulaAjuste", True))
        Me.CodCedulaAjusteTextBox.Location = New System.Drawing.Point(148, 313)
        Me.CodCedulaAjusteTextBox.Name = "CodCedulaAjusteTextBox"
        Me.CodCedulaAjusteTextBox.Size = New System.Drawing.Size(202, 20)
        Me.CodCedulaAjusteTextBox.TabIndex = 128
        '
        'TOTALAJUSTARTextBox
        '
        Me.TOTALAJUSTARTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AjustesBindingSource, "TOTALAJUSTAR", True))
        Me.TOTALAJUSTARTextBox.Location = New System.Drawing.Point(148, 339)
        Me.TOTALAJUSTARTextBox.Name = "TOTALAJUSTARTextBox"
        Me.TOTALAJUSTARTextBox.Size = New System.Drawing.Size(202, 20)
        Me.TOTALAJUSTARTextBox.TabIndex = 130
        '
        'CODIGO_DE_PUESTOTextBox
        '
        Me.CODIGO_DE_PUESTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "CODIGO DE PUESTO", True))
        Me.CODIGO_DE_PUESTOTextBox.Location = New System.Drawing.Point(148, 266)
        Me.CODIGO_DE_PUESTOTextBox.Name = "CODIGO_DE_PUESTOTextBox"
        Me.CODIGO_DE_PUESTOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.CODIGO_DE_PUESTOTextBox.TabIndex = 124
        '
        'SALARIO_UNICOTextBox
        '
        Me.SALARIO_UNICOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "SALARIO UNICO", True))
        Me.SALARIO_UNICOTextBox.Location = New System.Drawing.Point(148, 290)
        Me.SALARIO_UNICOTextBox.Name = "SALARIO_UNICOTextBox"
        Me.SALARIO_UNICOTextBox.Size = New System.Drawing.Size(202, 20)
        Me.SALARIO_UNICOTextBox.TabIndex = 126
        '
        'CodImpuestoTextBox
        '
        Me.CodImpuestoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "codImpuesto", True))
        Me.CodImpuestoTextBox.Location = New System.Drawing.Point(148, 365)
        Me.CodImpuestoTextBox.Name = "CodImpuestoTextBox"
        Me.CodImpuestoTextBox.Size = New System.Drawing.Size(202, 20)
        Me.CodImpuestoTextBox.TabIndex = 122
        '
        'CodDeduccionTextBox
        '
        Me.CodDeduccionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DeduccionesBindingSource, "codDeduccion", True))
        Me.CodDeduccionTextBox.Location = New System.Drawing.Point(148, 213)
        Me.CodDeduccionTextBox.Name = "CodDeduccionTextBox"
        Me.CodDeduccionTextBox.Size = New System.Drawing.Size(200, 20)
        Me.CodDeduccionTextBox.TabIndex = 118
        '
        'SumaTotalTextBox
        '
        Me.SumaTotalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DeduccionesBindingSource, "SumaTotal", True))
        Me.SumaTotalTextBox.Location = New System.Drawing.Point(148, 239)
        Me.SumaTotalTextBox.Name = "SumaTotalTextBox"
        Me.SumaTotalTextBox.Size = New System.Drawing.Size(200, 20)
        Me.SumaTotalTextBox.TabIndex = 120
        '
        'NoCEDULATextBox
        '
        Me.NoCEDULATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NoCEDULA", True))
        Me.NoCEDULATextBox.Location = New System.Drawing.Point(150, 26)
        Me.NoCEDULATextBox.Name = "NoCEDULATextBox"
        Me.NoCEDULATextBox.Size = New System.Drawing.Size(200, 20)
        Me.NoCEDULATextBox.TabIndex = 104
        '
        'NOMBRE_COMPLETOTextBox
        '
        Me.NOMBRE_COMPLETOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NOMBRE COMPLETO", True))
        Me.NOMBRE_COMPLETOTextBox.Location = New System.Drawing.Point(148, 57)
        Me.NOMBRE_COMPLETOTextBox.Name = "NOMBRE_COMPLETOTextBox"
        Me.NOMBRE_COMPLETOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.NOMBRE_COMPLETOTextBox.TabIndex = 106
        '
        'CODPUESTOTextBox
        '
        Me.CODPUESTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "CODPUESTO", True))
        Me.CODPUESTOTextBox.Location = New System.Drawing.Point(148, 83)
        Me.CODPUESTOTextBox.Name = "CODPUESTOTextBox"
        Me.CODPUESTOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.CODPUESTOTextBox.TabIndex = 108
        '
        'FECHA_INGRESODateTimePicker
        '
        Me.FECHA_INGRESODateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.DatosPersonalesBindingSource, "FECHA INGRESO", True))
        Me.FECHA_INGRESODateTimePicker.Location = New System.Drawing.Point(148, 109)
        Me.FECHA_INGRESODateTimePicker.Name = "FECHA_INGRESODateTimePicker"
        Me.FECHA_INGRESODateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.FECHA_INGRESODateTimePicker.TabIndex = 110
        '
        'AÑOSSERVTextBox
        '
        Me.AÑOSSERVTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "AÑOSSERV", True))
        Me.AÑOSSERVTextBox.Location = New System.Drawing.Point(148, 135)
        Me.AÑOSSERVTextBox.Name = "AÑOSSERVTextBox"
        Me.AÑOSSERVTextBox.Size = New System.Drawing.Size(200, 20)
        Me.AÑOSSERVTextBox.TabIndex = 112
        '
        'N__CONYUGESCheckBox
        '
        Me.N__CONYUGESCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.DatosPersonalesBindingSource, "N° CONYUGES", True))
        Me.N__CONYUGESCheckBox.Location = New System.Drawing.Point(148, 161)
        Me.N__CONYUGESCheckBox.Name = "N__CONYUGESCheckBox"
        Me.N__CONYUGESCheckBox.Size = New System.Drawing.Size(200, 24)
        Me.N__CONYUGESCheckBox.TabIndex = 114
        Me.N__CONYUGESCheckBox.Text = "CheckBox1"
        Me.N__CONYUGESCheckBox.UseVisualStyleBackColor = True
        '
        'N__HIJOSTextBox
        '
        Me.N__HIJOSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "N° HIJOS", True))
        Me.N__HIJOSTextBox.Location = New System.Drawing.Point(148, 187)
        Me.N__HIJOSTextBox.Name = "N__HIJOSTextBox"
        Me.N__HIJOSTextBox.Size = New System.Drawing.Size(200, 20)
        Me.N__HIJOSTextBox.TabIndex = 116
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Label1)
        Me.GroupBox2.Controls.Add(Me.FechaPlanMensualDateTimePicker)
        Me.GroupBox2.Controls.Add(__ANUALLabel)
        Me.GroupBox2.Controls.Add(Me.__ANUALTextBox)
        Me.GroupBox2.Controls.Add(ANUALIDADLabel)
        Me.GroupBox2.Controls.Add(Me.ANUALIDADTextBox)
        Me.GroupBox2.Controls.Add(SALARIO_DIARIOLabel)
        Me.GroupBox2.Controls.Add(Me.SALARIO_DIARIOTextBox)
        Me.GroupBox2.Controls.Add(AsientoLabel)
        Me.GroupBox2.Controls.Add(Me.AsientoTextBox)
        Me.GroupBox2.Controls.Add(ProcentajeImpuesto1Label)
        Me.GroupBox2.Controls.Add(IMP_RENTALabel)
        Me.GroupBox2.Controls.Add(Me.ProcentajeImpuesto1TextBox)
        Me.GroupBox2.Controls.Add(Me.IMP_RENTATextBox)
        Me.GroupBox2.Controls.Add(CESANTIALabel)
        Me.GroupBox2.Controls.Add(Me.CESANTIATextBox)
        Me.GroupBox2.Controls.Add(CodDedLabel)
        Me.GroupBox2.Controls.Add(Me.CodDedTextBox)
        Me.GroupBox2.Controls.Add(CCSSLabel)
        Me.GroupBox2.Controls.Add(Me.CCSSTextBox)
        Me.GroupBox2.Location = New System.Drawing.Point(383, 52)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(394, 282)
        Me.GroupBox2.TabIndex = 107
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "CalculosLaborales"
        '
        'FechaPlanMensualDateTimePicker
        '
        Me.FechaPlanMensualDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.PlanillaMensualBindingSource, "FechaPlanMensual", True))
        Me.FechaPlanMensualDateTimePicker.Location = New System.Drawing.Point(186, 23)
        Me.FechaPlanMensualDateTimePicker.Name = "FechaPlanMensualDateTimePicker"
        Me.FechaPlanMensualDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.FechaPlanMensualDateTimePicker.TabIndex = 1
        '
        'PlanillaMensualBindingSource
        '
        Me.PlanillaMensualBindingSource.DataMember = "PlanillaMensual"
        Me.PlanillaMensualBindingSource.DataSource = Me.Planilla2DataSet1
        '
        '__ANUALTextBox
        '
        Me.__ANUALTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "% ANUAL", True))
        Me.__ANUALTextBox.Location = New System.Drawing.Point(186, 53)
        Me.__ANUALTextBox.Name = "__ANUALTextBox"
        Me.__ANUALTextBox.Size = New System.Drawing.Size(200, 20)
        Me.__ANUALTextBox.TabIndex = 7
        '
        'ANUALIDADTextBox
        '
        Me.ANUALIDADTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "ANUALIDAD", True))
        Me.ANUALIDADTextBox.Location = New System.Drawing.Point(186, 79)
        Me.ANUALIDADTextBox.Name = "ANUALIDADTextBox"
        Me.ANUALIDADTextBox.Size = New System.Drawing.Size(200, 20)
        Me.ANUALIDADTextBox.TabIndex = 9
        '
        'SALARIO_DIARIOTextBox
        '
        Me.SALARIO_DIARIOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "SALARIO DIARIO", True))
        Me.SALARIO_DIARIOTextBox.Location = New System.Drawing.Point(186, 105)
        Me.SALARIO_DIARIOTextBox.Name = "SALARIO_DIARIOTextBox"
        Me.SALARIO_DIARIOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.SALARIO_DIARIOTextBox.TabIndex = 13
        '
        'AsientoTextBox
        '
        Me.AsientoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "Asiento", True))
        Me.AsientoTextBox.Location = New System.Drawing.Point(186, 231)
        Me.AsientoTextBox.Name = "AsientoTextBox"
        Me.AsientoTextBox.Size = New System.Drawing.Size(200, 20)
        Me.AsientoTextBox.TabIndex = 33
        '
        'IMP_RENTATextBox
        '
        Me.IMP_RENTATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "IMP RENTA", True))
        Me.IMP_RENTATextBox.Location = New System.Drawing.Point(186, 131)
        Me.IMP_RENTATextBox.Name = "IMP_RENTATextBox"
        Me.IMP_RENTATextBox.Size = New System.Drawing.Size(200, 20)
        Me.IMP_RENTATextBox.TabIndex = 35
        '
        'CESANTIATextBox
        '
        Me.CESANTIATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "CESANTIA", True))
        Me.CESANTIATextBox.Location = New System.Drawing.Point(186, 154)
        Me.CESANTIATextBox.Name = "CESANTIATextBox"
        Me.CESANTIATextBox.Size = New System.Drawing.Size(200, 20)
        Me.CESANTIATextBox.TabIndex = 39
        '
        'CodDedTextBox
        '
        Me.CodDedTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "codDed", True))
        Me.CodDedTextBox.Location = New System.Drawing.Point(186, 179)
        Me.CodDedTextBox.Name = "CodDedTextBox"
        Me.CodDedTextBox.Size = New System.Drawing.Size(200, 20)
        Me.CodDedTextBox.TabIndex = 43
        '
        'CCSSTextBox
        '
        Me.CCSSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "CCSS", True))
        Me.CCSSTextBox.Location = New System.Drawing.Point(186, 205)
        Me.CCSSTextBox.Name = "CCSSTextBox"
        Me.CCSSTextBox.Size = New System.Drawing.Size(200, 20)
        Me.CCSSTextBox.TabIndex = 45
        '
        'PlanillaMensualTableAdapter
        '
        Me.PlanillaMensualTableAdapter.ClearBeforeFill = True
        '
        'LIQUIDO_A_PAGARTextBox
        '
        Me.LIQUIDO_A_PAGARTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "LIQUIDO A PAGAR", True))
        Me.LIQUIDO_A_PAGARTextBox.Location = New System.Drawing.Point(167, 144)
        Me.LIQUIDO_A_PAGARTextBox.Name = "LIQUIDO_A_PAGARTextBox"
        Me.LIQUIDO_A_PAGARTextBox.Size = New System.Drawing.Size(200, 20)
        Me.LIQUIDO_A_PAGARTextBox.TabIndex = 15
        '
        'DIAS_TRABAJTextBox
        '
        Me.DIAS_TRABAJTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "DIAS TRABAJ", True))
        Me.DIAS_TRABAJTextBox.Location = New System.Drawing.Point(167, 40)
        Me.DIAS_TRABAJTextBox.Name = "DIAS_TRABAJTextBox"
        Me.DIAS_TRABAJTextBox.Size = New System.Drawing.Size(200, 20)
        Me.DIAS_TRABAJTextBox.TabIndex = 17
        '
        'HORAS_EXTRAS_REGULARTextBox
        '
        Me.HORAS_EXTRAS_REGULARTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "HORAS EXTRAS REGULAR", True))
        Me.HORAS_EXTRAS_REGULARTextBox.Location = New System.Drawing.Point(167, 66)
        Me.HORAS_EXTRAS_REGULARTextBox.Name = "HORAS_EXTRAS_REGULARTextBox"
        Me.HORAS_EXTRAS_REGULARTextBox.Size = New System.Drawing.Size(200, 20)
        Me.HORAS_EXTRAS_REGULARTextBox.TabIndex = 23
        '
        'HORAS_EXTRAS_DOBLESTextBox
        '
        Me.HORAS_EXTRAS_DOBLESTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "HORAS EXTRAS DOBLES", True))
        Me.HORAS_EXTRAS_DOBLESTextBox.Location = New System.Drawing.Point(167, 92)
        Me.HORAS_EXTRAS_DOBLESTextBox.Name = "HORAS_EXTRAS_DOBLESTextBox"
        Me.HORAS_EXTRAS_DOBLESTextBox.Size = New System.Drawing.Size(200, 20)
        Me.HORAS_EXTRAS_DOBLESTextBox.TabIndex = 25
        '
        'EXTRASTextBox
        '
        Me.EXTRASTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "EXTRAS", True))
        Me.EXTRASTextBox.Location = New System.Drawing.Point(167, 118)
        Me.EXTRASTextBox.Name = "EXTRASTextBox"
        Me.EXTRASTextBox.Size = New System.Drawing.Size(200, 20)
        Me.EXTRASTextBox.TabIndex = 37
        '
        'SUBSIDIOTextBox
        '
        Me.SUBSIDIOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "SUBSIDIO", True))
        Me.SUBSIDIOTextBox.Location = New System.Drawing.Point(159, 129)
        Me.SUBSIDIOTextBox.Name = "SUBSIDIOTextBox"
        Me.SUBSIDIOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.SUBSIDIOTextBox.TabIndex = 41
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.InstitucionIncapacidadTextBox)
        Me.GroupBox3.Controls.Add(TOTAL_DIAS_INCAPLabel)
        Me.GroupBox3.Controls.Add(Me.TOTAL_DIAS_INCAPTextBox)
        Me.GroupBox3.Controls.Add(FechaINCAPINILabel)
        Me.GroupBox3.Controls.Add(Me.FechaINCAPINIDateTimePicker)
        Me.GroupBox3.Controls.Add(FechaINCAPFINLabel)
        Me.GroupBox3.Controls.Add(Me.FechaINCAPFINDateTimePicker)
        Me.GroupBox3.Controls.Add(InstitucionIncapacidadLabel)
        Me.GroupBox3.Controls.Add(Me.SUBSIDIOTextBox)
        Me.GroupBox3.Controls.Add(SUBSIDIOLabel)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 453)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(365, 180)
        Me.GroupBox3.TabIndex = 108
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Incapacidad"
        '
        'InstitucionIncapacidadTextBox
        '
        Me.InstitucionIncapacidadTextBox.FormattingEnabled = True
        Me.InstitucionIncapacidadTextBox.Items.AddRange(New Object() {"CCSS", "INS"})
        Me.InstitucionIncapacidadTextBox.Location = New System.Drawing.Point(159, 28)
        Me.InstitucionIncapacidadTextBox.Name = "InstitucionIncapacidadTextBox"
        Me.InstitucionIncapacidadTextBox.Size = New System.Drawing.Size(200, 21)
        Me.InstitucionIncapacidadTextBox.TabIndex = 42
        '
        'TOTAL_DIAS_INCAPTextBox
        '
        Me.TOTAL_DIAS_INCAPTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "TOTAL DIAS INCAP", True))
        Me.TOTAL_DIAS_INCAPTextBox.Location = New System.Drawing.Point(159, 107)
        Me.TOTAL_DIAS_INCAPTextBox.Name = "TOTAL_DIAS_INCAPTextBox"
        Me.TOTAL_DIAS_INCAPTextBox.Size = New System.Drawing.Size(200, 20)
        Me.TOTAL_DIAS_INCAPTextBox.TabIndex = 33
        '
        'FechaINCAPINIDateTimePicker
        '
        Me.FechaINCAPINIDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.PlanillaMensualBindingSource, "FechaINCAPINI", True))
        Me.FechaINCAPINIDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaINCAPINIDateTimePicker.Location = New System.Drawing.Point(159, 56)
        Me.FechaINCAPINIDateTimePicker.Name = "FechaINCAPINIDateTimePicker"
        Me.FechaINCAPINIDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.FechaINCAPINIDateTimePicker.TabIndex = 35
        '
        'FechaINCAPFINDateTimePicker
        '
        Me.FechaINCAPFINDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.PlanillaMensualBindingSource, "FechaINCAPFIN", True))
        Me.FechaINCAPFINDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaINCAPFINDateTimePicker.Location = New System.Drawing.Point(161, 82)
        Me.FechaINCAPFINDateTimePicker.Name = "FechaINCAPFINDateTimePicker"
        Me.FechaINCAPFINDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.FechaINCAPFINDateTimePicker.TabIndex = 37
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.EXTRASTextBox)
        Me.GroupBox4.Controls.Add(EXTRASLabel)
        Me.GroupBox4.Controls.Add(Me.HORAS_EXTRAS_DOBLESTextBox)
        Me.GroupBox4.Controls.Add(HORAS_EXTRAS_DOBLESLabel)
        Me.GroupBox4.Controls.Add(Me.HORAS_EXTRAS_REGULARTextBox)
        Me.GroupBox4.Controls.Add(HORAS_EXTRAS_REGULARLabel)
        Me.GroupBox4.Controls.Add(Me.DIAS_TRABAJTextBox)
        Me.GroupBox4.Controls.Add(DIAS_TRABAJLabel)
        Me.GroupBox4.Controls.Add(Me.LIQUIDO_A_PAGARTextBox)
        Me.GroupBox4.Controls.Add(LIQUIDO_A_PAGARLabel)
        Me.GroupBox4.Location = New System.Drawing.Point(383, 342)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(394, 204)
        Me.GroupBox4.TabIndex = 46
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "DatoLaborales"
        '
        'Button1
        '
        Me.Button1.Image = Global.PlanillasProyecto3.My.Resources.Resources.guardar_documento_icono_7840_48
        Me.Button1.Location = New System.Drawing.Point(393, 557)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(84, 68)
        Me.Button1.TabIndex = 113
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Image = Global.PlanillasProyecto3.My.Resources.Resources.Eliminar
        Me.Button3.Location = New System.Drawing.Point(592, 554)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(68, 71)
        Me.Button3.TabIndex = 115
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Image = Global.PlanillasProyecto3.My.Resources.Resources.icono_retroceso
        Me.Button4.Location = New System.Drawing.Point(685, 554)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 71)
        Me.Button4.TabIndex = 116
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Image = Global.PlanillasProyecto3.My.Resources.Resources.iconoBuscarAutor
        Me.Button2.Location = New System.Drawing.Point(492, 557)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(81, 68)
        Me.Button2.TabIndex = 114
        Me.Button2.UseVisualStyleBackColor = True
        '
        'frmPlanillaMensuals
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(785, 674)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(CreditosFiscalesHijoLabel)
        Me.Controls.Add(Me.CreditosFiscalesConyugeTextBox)
        Me.Controls.Add(CreditosFiscalesConyugeLabel)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmPlanillaMensuals"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmPlanillaMensuals"
        CType(Me.Planilla2DataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatosPersonalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DeduccionesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImpuestosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PuestosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AjustesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.PlanillaMensualBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Planilla2DataSet1 As PlanillasProyecto3.Planilla2DataSet1
    Friend WithEvents DatosPersonalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DatosPersonalesTableAdapter As PlanillasProyecto3.Planilla2DataSet1TableAdapters.DatosPersonalesTableAdapter
    Friend WithEvents TableAdapterManager As PlanillasProyecto3.Planilla2DataSet1TableAdapters.TableAdapterManager
    Friend WithEvents DeduccionesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DeduccionesTableAdapter As PlanillasProyecto3.Planilla2DataSet1TableAdapters.DeduccionesTableAdapter
    Friend WithEvents ImpuestosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ImpuestosTableAdapter As PlanillasProyecto3.Planilla2DataSet1TableAdapters.ImpuestosTableAdapter
    Friend WithEvents ProcentajeImpuesto1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents ProcentajeImpuesto2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents CreditosFiscalesHijoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CreditosFiscalesConyugeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PuestosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PuestosTableAdapter As PlanillasProyecto3.Planilla2DataSet1TableAdapters.PuestosTableAdapter
    Friend WithEvents AjustesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AjustesTableAdapter As PlanillasProyecto3.Planilla2DataSet1TableAdapters.AjustesTableAdapter
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ArchivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegresarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CodCedulaAjusteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TOTALAJUSTARTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CODIGO_DE_PUESTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALARIO_UNICOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CodImpuestoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CodDeduccionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SumaTotalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NoCEDULATextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRE_COMPLETOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CODPUESTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FECHA_INGRESODateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents AÑOSSERVTextBox As System.Windows.Forms.TextBox
    Friend WithEvents N__CONYUGESCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents N__HIJOSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents PlanillaMensualBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PlanillaMensualTableAdapter As PlanillasProyecto3.Planilla2DataSet1TableAdapters.PlanillaMensualTableAdapter
    Friend WithEvents FechaPlanMensualDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents __ANUALTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ANUALIDADTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALARIO_DIARIOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents LIQUIDO_A_PAGARTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DIAS_TRABAJTextBox As System.Windows.Forms.TextBox
    Friend WithEvents HORAS_EXTRAS_REGULARTextBox As System.Windows.Forms.TextBox
    Friend WithEvents HORAS_EXTRAS_DOBLESTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AsientoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents IMP_RENTATextBox As System.Windows.Forms.TextBox
    Friend WithEvents EXTRASTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CESANTIATextBox As System.Windows.Forms.TextBox
    Friend WithEvents SUBSIDIOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CodDedTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CCSSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents TOTAL_DIAS_INCAPTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FechaINCAPINIDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents FechaINCAPFINDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents InstitucionIncapacidadTextBox As System.Windows.Forms.ComboBox
End Class
