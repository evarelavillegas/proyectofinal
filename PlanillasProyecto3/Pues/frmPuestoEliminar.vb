﻿Public Class frmPuestoEliminar

    Public Shared codPuesto As Integer

    Private Sub PuestosBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.PuestosBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmPuestoEliminar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.Puestos' Puede moverla o quitarla según sea necesario.
        'Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)
        txtDatoConsulta.Focus()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click

        Try
            Me.PuestosTableAdapter.FillByBuscarPorCodigo(Me.Planilla2DataSet.Puestos, Integer.Parse(txtDatoConsulta.Text))

            If MsgBox("¿ESTÁ SEGURO DE ELIMINAR ESTE REGISTRO?", MsgBoxStyle.OkCancel, "CONFIRMAR") = MsgBoxResult.Ok Then

                Dim codPuesto As Integer = Integer.Parse(txtDatoConsulta.Text)
                Dim lleno As Integer = 0
                lleno = Me.PuestosTableAdapter.buscarPuesto(Me.Planilla2DataSet.Puestos, codPuesto)

                If lleno <> 0 Then
                    Me.PuestosTableAdapter.DeletePuesto(codPuesto)
                    MsgBox("Se Elimino satisfactoriamente La persona")
                    Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)
                Else
                    MsgBox("No se Ecuentra Ninguana Cedula Guarda, Compruebe En el Boton De Consulta")
                End If
            End If
        Catch ex As Exception
            MsgBox(" Verifique Que la Cedula sea Númerica")
        End Try

    End Sub

    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        frmPuestosActualizar.tipoFormularioPuestoValidar = "Eliminar"
        frmPuestosConsulta.Show()
        Me.Hide()
    End Sub
End Class