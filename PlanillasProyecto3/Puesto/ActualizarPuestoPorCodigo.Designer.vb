﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ActualizarPuestoPorCodigo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CODIGO_DE_PUESTOLabel As System.Windows.Forms.Label
        Dim DESCRIPCIONLabel As System.Windows.Forms.Label
        Dim DEPARTAMENTOLabel As System.Windows.Forms.Label
        Dim SALARIO_UNICOLabel As System.Windows.Forms.Label
        Dim EMPLEADO_CONFIANZALabel As System.Windows.Forms.Label
        Dim GRADO_ACADEMICOLabel As System.Windows.Forms.Label
        Me.Planilla2DataSet = New Proyecto3.Planilla2DataSet()
        Me.PuestosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PuestosTableAdapter = New Proyecto3.Planilla2DataSetTableAdapters.PuestosTableAdapter()
        Me.TableAdapterManager = New Proyecto3.Planilla2DataSetTableAdapters.TableAdapterManager()
        Me.CODIGO_DE_PUESTOTextBox = New System.Windows.Forms.TextBox()
        Me.DESCRIPCIONTextBox = New System.Windows.Forms.TextBox()
        Me.DEPARTAMENTOTextBox = New System.Windows.Forms.TextBox()
        Me.SALARIO_UNICOTextBox = New System.Windows.Forms.TextBox()
        Me.EMPLEADO_CONFIANZACheckBox = New System.Windows.Forms.CheckBox()
        Me.GRADO_ACADEMICOTextBox = New System.Windows.Forms.TextBox()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.proveedorError = New System.Windows.Forms.ErrorProvider(Me.components)
        CODIGO_DE_PUESTOLabel = New System.Windows.Forms.Label()
        DESCRIPCIONLabel = New System.Windows.Forms.Label()
        DEPARTAMENTOLabel = New System.Windows.Forms.Label()
        SALARIO_UNICOLabel = New System.Windows.Forms.Label()
        EMPLEADO_CONFIANZALabel = New System.Windows.Forms.Label()
        GRADO_ACADEMICOLabel = New System.Windows.Forms.Label()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PuestosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.proveedorError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CODIGO_DE_PUESTOLabel
        '
        CODIGO_DE_PUESTOLabel.AutoSize = True
        CODIGO_DE_PUESTOLabel.Location = New System.Drawing.Point(163, 58)
        CODIGO_DE_PUESTOLabel.Name = "CODIGO_DE_PUESTOLabel"
        CODIGO_DE_PUESTOLabel.Size = New System.Drawing.Size(117, 13)
        CODIGO_DE_PUESTOLabel.TabIndex = 13
        CODIGO_DE_PUESTOLabel.Text = "CODIGO DE PUESTO:"
        '
        'DESCRIPCIONLabel
        '
        DESCRIPCIONLabel.AutoSize = True
        DESCRIPCIONLabel.Location = New System.Drawing.Point(163, 84)
        DESCRIPCIONLabel.Name = "DESCRIPCIONLabel"
        DESCRIPCIONLabel.Size = New System.Drawing.Size(83, 13)
        DESCRIPCIONLabel.TabIndex = 15
        DESCRIPCIONLabel.Text = "DESCRIPCION:"
        '
        'DEPARTAMENTOLabel
        '
        DEPARTAMENTOLabel.AutoSize = True
        DEPARTAMENTOLabel.Location = New System.Drawing.Point(163, 110)
        DEPARTAMENTOLabel.Name = "DEPARTAMENTOLabel"
        DEPARTAMENTOLabel.Size = New System.Drawing.Size(100, 13)
        DEPARTAMENTOLabel.TabIndex = 17
        DEPARTAMENTOLabel.Text = "DEPARTAMENTO:"
        '
        'SALARIO_UNICOLabel
        '
        SALARIO_UNICOLabel.AutoSize = True
        SALARIO_UNICOLabel.Location = New System.Drawing.Point(163, 136)
        SALARIO_UNICOLabel.Name = "SALARIO_UNICOLabel"
        SALARIO_UNICOLabel.Size = New System.Drawing.Size(93, 13)
        SALARIO_UNICOLabel.TabIndex = 19
        SALARIO_UNICOLabel.Text = "SALARIO UNICO:"
        '
        'EMPLEADO_CONFIANZALabel
        '
        EMPLEADO_CONFIANZALabel.AutoSize = True
        EMPLEADO_CONFIANZALabel.Location = New System.Drawing.Point(163, 164)
        EMPLEADO_CONFIANZALabel.Name = "EMPLEADO_CONFIANZALabel"
        EMPLEADO_CONFIANZALabel.Size = New System.Drawing.Size(133, 13)
        EMPLEADO_CONFIANZALabel.TabIndex = 21
        EMPLEADO_CONFIANZALabel.Text = "EMPLEADO CONFIANZA:"
        '
        'GRADO_ACADEMICOLabel
        '
        GRADO_ACADEMICOLabel.AutoSize = True
        GRADO_ACADEMICOLabel.Location = New System.Drawing.Point(163, 192)
        GRADO_ACADEMICOLabel.Name = "GRADO_ACADEMICOLabel"
        GRADO_ACADEMICOLabel.Size = New System.Drawing.Size(115, 13)
        GRADO_ACADEMICOLabel.TabIndex = 23
        GRADO_ACADEMICOLabel.Text = "GRADO ACADEMICO:"
        '
        'Planilla2DataSet
        '
        Me.Planilla2DataSet.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PuestosBindingSource
        '
        Me.PuestosBindingSource.DataMember = "Puestos"
        Me.PuestosBindingSource.DataSource = Me.Planilla2DataSet
        '
        'PuestosTableAdapter
        '
        Me.PuestosTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AjustesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Nothing
        Me.TableAdapterManager.DeduccionCooperativaTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager.PuestosTableAdapter = Me.PuestosTableAdapter
        Me.TableAdapterManager.UpdateOrder = Proyecto3.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'CODIGO_DE_PUESTOTextBox
        '
        Me.CODIGO_DE_PUESTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "CODIGO DE PUESTO", True))
        Me.CODIGO_DE_PUESTOTextBox.Location = New System.Drawing.Point(302, 55)
        Me.CODIGO_DE_PUESTOTextBox.Name = "CODIGO_DE_PUESTOTextBox"
        Me.CODIGO_DE_PUESTOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.CODIGO_DE_PUESTOTextBox.TabIndex = 14
        '
        'DESCRIPCIONTextBox
        '
        Me.DESCRIPCIONTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "DESCRIPCION", True))
        Me.DESCRIPCIONTextBox.Location = New System.Drawing.Point(302, 81)
        Me.DESCRIPCIONTextBox.Name = "DESCRIPCIONTextBox"
        Me.DESCRIPCIONTextBox.Size = New System.Drawing.Size(104, 20)
        Me.DESCRIPCIONTextBox.TabIndex = 16
        '
        'DEPARTAMENTOTextBox
        '
        Me.DEPARTAMENTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "DEPARTAMENTO", True))
        Me.DEPARTAMENTOTextBox.Location = New System.Drawing.Point(302, 107)
        Me.DEPARTAMENTOTextBox.Name = "DEPARTAMENTOTextBox"
        Me.DEPARTAMENTOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.DEPARTAMENTOTextBox.TabIndex = 18
        '
        'SALARIO_UNICOTextBox
        '
        Me.SALARIO_UNICOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "SALARIO UNICO", True))
        Me.SALARIO_UNICOTextBox.Location = New System.Drawing.Point(302, 133)
        Me.SALARIO_UNICOTextBox.Name = "SALARIO_UNICOTextBox"
        Me.SALARIO_UNICOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.SALARIO_UNICOTextBox.TabIndex = 20
        '
        'EMPLEADO_CONFIANZACheckBox
        '
        Me.EMPLEADO_CONFIANZACheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.PuestosBindingSource, "EMPLEADO CONFIANZA", True))
        Me.EMPLEADO_CONFIANZACheckBox.Location = New System.Drawing.Point(302, 159)
        Me.EMPLEADO_CONFIANZACheckBox.Name = "EMPLEADO_CONFIANZACheckBox"
        Me.EMPLEADO_CONFIANZACheckBox.Size = New System.Drawing.Size(104, 24)
        Me.EMPLEADO_CONFIANZACheckBox.TabIndex = 22
        Me.EMPLEADO_CONFIANZACheckBox.Text = "Si"
        Me.EMPLEADO_CONFIANZACheckBox.UseVisualStyleBackColor = True
        '
        'GRADO_ACADEMICOTextBox
        '
        Me.GRADO_ACADEMICOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "GRADO ACADEMICO", True))
        Me.GRADO_ACADEMICOTextBox.Location = New System.Drawing.Point(302, 189)
        Me.GRADO_ACADEMICOTextBox.Name = "GRADO_ACADEMICOTextBox"
        Me.GRADO_ACADEMICOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.GRADO_ACADEMICOTextBox.TabIndex = 24
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(465, 121)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(82, 40)
        Me.btnGuardar.TabIndex = 25
        Me.btnGuardar.Text = "&Guardar Actualizacion"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'proveedorError
        '
        Me.proveedorError.ContainerControl = Me
        '
        'ActualizarPuestoPorCodigo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(568, 264)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(CODIGO_DE_PUESTOLabel)
        Me.Controls.Add(Me.CODIGO_DE_PUESTOTextBox)
        Me.Controls.Add(DESCRIPCIONLabel)
        Me.Controls.Add(Me.DESCRIPCIONTextBox)
        Me.Controls.Add(DEPARTAMENTOLabel)
        Me.Controls.Add(Me.DEPARTAMENTOTextBox)
        Me.Controls.Add(SALARIO_UNICOLabel)
        Me.Controls.Add(Me.SALARIO_UNICOTextBox)
        Me.Controls.Add(EMPLEADO_CONFIANZALabel)
        Me.Controls.Add(Me.EMPLEADO_CONFIANZACheckBox)
        Me.Controls.Add(GRADO_ACADEMICOLabel)
        Me.Controls.Add(Me.GRADO_ACADEMICOTextBox)
        Me.Name = "ActualizarPuestoPorCodigo"
        Me.Text = "Actualizar Puesto Por Codigo"
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PuestosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.proveedorError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Planilla2DataSet As Proyecto3.Planilla2DataSet
    Friend WithEvents PuestosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PuestosTableAdapter As Proyecto3.Planilla2DataSetTableAdapters.PuestosTableAdapter
    Friend WithEvents TableAdapterManager As Proyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents CODIGO_DE_PUESTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DESCRIPCIONTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DEPARTAMENTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALARIO_UNICOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EMPLEADO_CONFIANZACheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents GRADO_ACADEMICOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents proveedorError As System.Windows.Forms.ErrorProvider
End Class
