﻿Public Class frmConsultaUsuario
    Public Shared tipoBusqueda As String
   

    Private Sub UsuarioRegistroBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.UsuarioRegistroBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmConsultaUsuario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.UsuarioRegistro' Puede moverla o quitarla según sea necesario.
        Me.UsuarioRegistroTableAdapter.Fill(Me.Planilla2DataSet.UsuarioRegistro)

    End Sub

    Private Sub UsuarioRegistroDataGridView_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmPrincipal.contra = UsuarioRegistroDataGridView1.Item(0, UsuarioRegistroDataGridView1.CurrentRow.Index).Value
        Me.Hide()
        MsgBox("Ingrese los Datos Para Actualizar")
        formOpciones_Usuario.btnOperacion.Text = "&Actualizar Usuario"
        formOpciones_Usuario.btnConsultar.Visible = False
        formOpciones_Usuario.Show()
        frmPrincipal.tipoOpcionDigitada = "Actualizar"
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Dim tipoBusqueda As Integer = Integer.Parse(cmbTipo.SelectedIndex)


        Try
            UsuarioRegistroTableAdapter.FillBy1(Me.Planilla2DataSet.UsuarioRegistro, txtDato.Text, txtDato.Text, txtDato.Text)
        Catch ex As Exception
            MsgBox("No se Encontro Ningun Registro")
        End Try

       

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.UsuarioRegistroTableAdapter.Fill(Me.Planilla2DataSet.UsuarioRegistro)

    End Sub

   
End Class