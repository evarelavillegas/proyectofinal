﻿Public Class formOpciones_Usuario


    ' Public Shared tipoEmpleado As Boolean
    
    Private Sub btnOperacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOperacion.Click
       
        Select Case frmPrincipal.tipoOpcionDigitada

            Case "Ingresar"
                Try
                    Me.UsuarioRegistroTableAdapter.InsertQuery(txtUsuario.Text, txtContrasena.Text, cmbConfiable.SelectedItem.ToString)
                    Me.UsuarioRegistroTableAdapter.Fill(Me.Planilla2DataSet.UsuarioRegistro)
                    MsgBox("Se Guardo Correctamente Los Datos")
                Catch ex As Exception
                    MsgBox("No se Puede Guardar Porque Existe Un Usuario Con la Misma Contraseña")
                End Try
            Case "Eliminar"

                If MsgBox("Esta Seguro De Eliminar Este Usuario", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    Try
                        If Me.UsuarioRegistroTableAdapter.DeleteUsuario(txtContrasena.Text) = "1" Then
                            MsgBox("Se Elimino El Usuario Correctamente")
                            txtContrasena.Clear()
                            txtUsuario.Clear()

                        Else
                            MsgBox("No se Encontro Ningun Usuario Con esta Contraseña, Consulte En el Boton de Consultas")
                        End If


                    Catch ex As Exception
                        MsgBox("No se Encontro Ningun Usuario Con esta Contraseña, Consulte En el Boton de Consultas")
                    End Try
                End If

            Case "Actualizar"

                Try
                    Me.UsuarioRegistroTableAdapter.UpdatePersona(txtUsuario.Text, txtContrasena.Text, cmbConfiable.SelectedItem.ToString, frmPrincipal.contra)
                    MsgBox("Se Actualizo Corectamente los datos del Usuario ")
                Catch ex As Exception
                    MsgBox("No se Realizo Los Cambios Correctamente")
                End Try

            Case Else

        End Select


    End Sub

    Private Sub formOpciones_Usuario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.UsuarioRegistro' Puede moverla o quitarla según sea necesario.
        Me.UsuarioRegistroTableAdapter.Fill(Me.Planilla2DataSet.UsuarioRegistro)

    End Sub

    Private Sub UsuarioRegistroBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.UsuarioRegistroBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub
End Class