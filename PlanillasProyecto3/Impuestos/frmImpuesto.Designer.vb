﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImpuesto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CodImpuestoLabel As System.Windows.Forms.Label
        Dim ProcentajeImpuesto1Label As System.Windows.Forms.Label
        Dim ProcentajeImpuesto2Label As System.Windows.Forms.Label
        Dim CreditosFiscalesHijoLabel As System.Windows.Forms.Label
        Dim CreditosFiscalesConyugeLabel As System.Windows.Forms.Label
        Me.Planilla2DataSet = New PlanillasProyecto3.Planilla2DataSet()
        Me.ImpuestosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ImpuestosTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.ImpuestosTableAdapter()
        Me.TableAdapterManager = New PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.CodImpuestoTextBox = New System.Windows.Forms.TextBox()
        Me.ProcentajeImpuesto1TextBox = New System.Windows.Forms.TextBox()
        Me.ProcentajeImpuesto2TextBox = New System.Windows.Forms.TextBox()
        Me.CreditosFiscalesHijoTextBox = New System.Windows.Forms.TextBox()
        Me.CreditosFiscalesConyugeTextBox = New System.Windows.Forms.TextBox()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton5 = New System.Windows.Forms.ToolStripButton()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegesarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GuardarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MostarHistorialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        CodImpuestoLabel = New System.Windows.Forms.Label()
        ProcentajeImpuesto1Label = New System.Windows.Forms.Label()
        ProcentajeImpuesto2Label = New System.Windows.Forms.Label()
        CreditosFiscalesHijoLabel = New System.Windows.Forms.Label()
        CreditosFiscalesConyugeLabel = New System.Windows.Forms.Label()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImpuestosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CodImpuestoLabel
        '
        CodImpuestoLabel.AutoSize = True
        CodImpuestoLabel.Location = New System.Drawing.Point(23, 35)
        CodImpuestoLabel.Name = "CodImpuestoLabel"
        CodImpuestoLabel.Size = New System.Drawing.Size(107, 13)
        CodImpuestoLabel.TabIndex = 14
        CodImpuestoLabel.Text = "Codigo de  Impuesto:"
        '
        'ProcentajeImpuesto1Label
        '
        ProcentajeImpuesto1Label.AutoSize = True
        ProcentajeImpuesto1Label.Location = New System.Drawing.Point(18, 61)
        ProcentajeImpuesto1Label.Name = "ProcentajeImpuesto1Label"
        ProcentajeImpuesto1Label.Size = New System.Drawing.Size(160, 13)
        ProcentajeImpuesto1Label.TabIndex = 16
        ProcentajeImpuesto1Label.Text = "Sobre el exceso de Impuesto #1"
        '
        'ProcentajeImpuesto2Label
        '
        ProcentajeImpuesto2Label.AutoSize = True
        ProcentajeImpuesto2Label.Location = New System.Drawing.Point(23, 87)
        ProcentajeImpuesto2Label.Name = "ProcentajeImpuesto2Label"
        ProcentajeImpuesto2Label.Size = New System.Drawing.Size(160, 13)
        ProcentajeImpuesto2Label.TabIndex = 18
        ProcentajeImpuesto2Label.Text = "Sobre el exceso de Impuesto #2"
        '
        'CreditosFiscalesHijoLabel
        '
        CreditosFiscalesHijoLabel.AutoSize = True
        CreditosFiscalesHijoLabel.Location = New System.Drawing.Point(23, 113)
        CreditosFiscalesHijoLabel.Name = "CreditosFiscalesHijoLabel"
        CreditosFiscalesHijoLabel.Size = New System.Drawing.Size(110, 13)
        CreditosFiscalesHijoLabel.TabIndex = 20
        CreditosFiscalesHijoLabel.Text = "Creditos Fiscales Hijo:"
        '
        'CreditosFiscalesConyugeLabel
        '
        CreditosFiscalesConyugeLabel.AutoSize = True
        CreditosFiscalesConyugeLabel.Location = New System.Drawing.Point(23, 139)
        CreditosFiscalesConyugeLabel.Name = "CreditosFiscalesConyugeLabel"
        CreditosFiscalesConyugeLabel.Size = New System.Drawing.Size(134, 13)
        CreditosFiscalesConyugeLabel.TabIndex = 22
        CreditosFiscalesConyugeLabel.Text = "Creditos Fiscales Conyuge:"
        '
        'Planilla2DataSet
        '
        Me.Planilla2DataSet.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ImpuestosBindingSource
        '
        Me.ImpuestosBindingSource.DataMember = "Impuestos"
        Me.ImpuestosBindingSource.DataSource = Me.Planilla2DataSet
        '
        'ImpuestosTableAdapter
        '
        Me.ImpuestosTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AjustesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Nothing
        Me.TableAdapterManager.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager.ImpuestosTableAdapter = Me.ImpuestosTableAdapter
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager.PuestosTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnEliminar)
        Me.GroupBox1.Controls.Add(Me.btnModificar)
        Me.GroupBox1.Controls.Add(Me.btnGuardar)
        Me.GroupBox1.Controls.Add(CodImpuestoLabel)
        Me.GroupBox1.Controls.Add(Me.CodImpuestoTextBox)
        Me.GroupBox1.Controls.Add(ProcentajeImpuesto1Label)
        Me.GroupBox1.Controls.Add(Me.ProcentajeImpuesto1TextBox)
        Me.GroupBox1.Controls.Add(ProcentajeImpuesto2Label)
        Me.GroupBox1.Controls.Add(Me.ProcentajeImpuesto2TextBox)
        Me.GroupBox1.Controls.Add(CreditosFiscalesHijoLabel)
        Me.GroupBox1.Controls.Add(Me.CreditosFiscalesHijoTextBox)
        Me.GroupBox1.Controls.Add(CreditosFiscalesConyugeLabel)
        Me.GroupBox1.Controls.Add(Me.CreditosFiscalesConyugeTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(21, 52)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(312, 231)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Impuestos Fiscales"
        '
        'btnEliminar
        '
        Me.btnEliminar.Image = Global.PlanillasProyecto3.My.Resources.Resources.Eliminar
        Me.btnEliminar.Location = New System.Drawing.Point(213, 162)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(73, 54)
        Me.btnEliminar.TabIndex = 26
        Me.ToolTip1.SetToolTip(Me.btnEliminar, "Eliminar")
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Image = Global.PlanillasProyecto3.My.Resources.Resources.icono_modificar
        Me.btnModificar.Location = New System.Drawing.Point(130, 162)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(75, 54)
        Me.btnModificar.TabIndex = 25
        Me.ToolTip1.SetToolTip(Me.btnModificar, "Modificar")
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Image = Global.PlanillasProyecto3.My.Resources.Resources.guardar_documento_icono_7840_48
        Me.btnGuardar.Location = New System.Drawing.Point(49, 162)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 54)
        Me.btnGuardar.TabIndex = 24
        Me.ToolTip1.SetToolTip(Me.btnGuardar, "Guardar")
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'CodImpuestoTextBox
        '
        Me.CodImpuestoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "codImpuesto", True))
        Me.CodImpuestoTextBox.Location = New System.Drawing.Point(186, 32)
        Me.CodImpuestoTextBox.Name = "CodImpuestoTextBox"
        Me.CodImpuestoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CodImpuestoTextBox.TabIndex = 15
        Me.ToolTip1.SetToolTip(Me.CodImpuestoTextBox, "Año impuesto fiscal")
        '
        'ProcentajeImpuesto1TextBox
        '
        Me.ProcentajeImpuesto1TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "procentajeImpuesto1", True))
        Me.ProcentajeImpuesto1TextBox.Location = New System.Drawing.Point(186, 58)
        Me.ProcentajeImpuesto1TextBox.Name = "ProcentajeImpuesto1TextBox"
        Me.ProcentajeImpuesto1TextBox.Size = New System.Drawing.Size(100, 20)
        Me.ProcentajeImpuesto1TextBox.TabIndex = 17
        Me.ToolTip1.SetToolTip(Me.ProcentajeImpuesto1TextBox, "Porcentaje sobre el sueldo #1")
        '
        'ProcentajeImpuesto2TextBox
        '
        Me.ProcentajeImpuesto2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "procentajeImpuesto2", True))
        Me.ProcentajeImpuesto2TextBox.Location = New System.Drawing.Point(186, 84)
        Me.ProcentajeImpuesto2TextBox.Name = "ProcentajeImpuesto2TextBox"
        Me.ProcentajeImpuesto2TextBox.Size = New System.Drawing.Size(100, 20)
        Me.ProcentajeImpuesto2TextBox.TabIndex = 19
        Me.ToolTip1.SetToolTip(Me.ProcentajeImpuesto2TextBox, "Porcentaje sobre el sueldo #2")
        '
        'CreditosFiscalesHijoTextBox
        '
        Me.CreditosFiscalesHijoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "creditosFiscalesHijo", True))
        Me.CreditosFiscalesHijoTextBox.Location = New System.Drawing.Point(186, 110)
        Me.CreditosFiscalesHijoTextBox.Name = "CreditosFiscalesHijoTextBox"
        Me.CreditosFiscalesHijoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CreditosFiscalesHijoTextBox.TabIndex = 21
        Me.ToolTip1.SetToolTip(Me.CreditosFiscalesHijoTextBox, "Deducción al impuesto por hijos")
        '
        'CreditosFiscalesConyugeTextBox
        '
        Me.CreditosFiscalesConyugeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "creditosFiscalesConyuge", True))
        Me.CreditosFiscalesConyugeTextBox.Location = New System.Drawing.Point(186, 136)
        Me.CreditosFiscalesConyugeTextBox.Name = "CreditosFiscalesConyugeTextBox"
        Me.CreditosFiscalesConyugeTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CreditosFiscalesConyugeTextBox.TabIndex = 23
        Me.ToolTip1.SetToolTip(Me.CreditosFiscalesConyugeTextBox, "Deducción al impuesto por conyugue")
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripButton2, Me.ToolStripButton3, Me.ToolStripButton4, Me.ToolStripButton5, Me.ToolStripButton6})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(375, 25)
        Me.ToolStrip1.TabIndex = 16
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = Global.PlanillasProyecto3.My.Resources.Resources.guardar_documento_icono_7840_48
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "Guardar"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.Image = Global.PlanillasProyecto3.My.Resources.Resources.icono_modificar
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton2.Text = "Modificar"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton3.Image = Global.PlanillasProyecto3.My.Resources.Resources.Eliminar
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton3.Text = "Eliminar"
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton4.Image = Global.PlanillasProyecto3.My.Resources.Resources.iconoBuscarAutor
        Me.ToolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton4.Text = "Buscar Reguistros"
        '
        'ToolStripButton5
        '
        Me.ToolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton5.Image = Global.PlanillasProyecto3.My.Resources.Resources.RegisterIcon1
        Me.ToolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton5.Name = "ToolStripButton5"
        Me.ToolStripButton5.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton5.Text = "Nuevo registro"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 304)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(375, 22)
        Me.StatusStrip1.TabIndex = 17
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(39, 17)
        Me.ToolStripStatusLabel1.Text = "Status"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivoToolStripMenuItem, Me.AccionesToolStripMenuItem, Me.ConsultarToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(375, 24)
        Me.MenuStrip1.TabIndex = 18
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ArchivoToolStripMenuItem
        '
        Me.ArchivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegesarToolStripMenuItem})
        Me.ArchivoToolStripMenuItem.Name = "ArchivoToolStripMenuItem"
        Me.ArchivoToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.ArchivoToolStripMenuItem.Text = "&Archivo"
        '
        'RegesarToolStripMenuItem
        '
        Me.RegesarToolStripMenuItem.Name = "RegesarToolStripMenuItem"
        Me.RegesarToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F), System.Windows.Forms.Keys)
        Me.RegesarToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.RegesarToolStripMenuItem.Text = "&Regesar"
        '
        'AccionesToolStripMenuItem
        '
        Me.AccionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GuardarToolStripMenuItem, Me.ModificarToolStripMenuItem, Me.EliminarToolStripMenuItem, Me.EliminarToolStripMenuItem1})
        Me.AccionesToolStripMenuItem.Name = "AccionesToolStripMenuItem"
        Me.AccionesToolStripMenuItem.Size = New System.Drawing.Size(67, 20)
        Me.AccionesToolStripMenuItem.Text = "&Acciones"
        '
        'GuardarToolStripMenuItem
        '
        Me.GuardarToolStripMenuItem.Name = "GuardarToolStripMenuItem"
        Me.GuardarToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.GuardarToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.GuardarToolStripMenuItem.Text = "&Guardar"
        '
        'ModificarToolStripMenuItem
        '
        Me.ModificarToolStripMenuItem.Name = "ModificarToolStripMenuItem"
        Me.ModificarToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.ModificarToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.ModificarToolStripMenuItem.Text = "&Modificar"
        '
        'EliminarToolStripMenuItem
        '
        Me.EliminarToolStripMenuItem.Name = "EliminarToolStripMenuItem"
        Me.EliminarToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.T), System.Windows.Forms.Keys)
        Me.EliminarToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.EliminarToolStripMenuItem.Text = "&Nuevo Registro"
        '
        'EliminarToolStripMenuItem1
        '
        Me.EliminarToolStripMenuItem1.Name = "EliminarToolStripMenuItem1"
        Me.EliminarToolStripMenuItem1.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.EliminarToolStripMenuItem1.Size = New System.Drawing.Size(196, 22)
        Me.EliminarToolStripMenuItem1.Text = "&Eliminar"
        '
        'ConsultarToolStripMenuItem
        '
        Me.ConsultarToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MostarHistorialToolStripMenuItem})
        Me.ConsultarToolStripMenuItem.Name = "ConsultarToolStripMenuItem"
        Me.ConsultarToolStripMenuItem.Size = New System.Drawing.Size(70, 20)
        Me.ConsultarToolStripMenuItem.Text = "&Consultar"
        '
        'MostarHistorialToolStripMenuItem
        '
        Me.MostarHistorialToolStripMenuItem.Name = "MostarHistorialToolStripMenuItem"
        Me.MostarHistorialToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.MostarHistorialToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.MostarHistorialToolStripMenuItem.Text = "Mostar Historial"
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton6.Image = Global.PlanillasProyecto3.My.Resources.Resources.icono_retroceso
        Me.ToolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton6.Text = "Regresar"
        '
        'frmImpuesto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(375, 326)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "frmImpuesto"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ingresar datos Impuestos"
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImpuestosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Planilla2DataSet As PlanillasProyecto3.Planilla2DataSet
    Friend WithEvents ImpuestosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ImpuestosTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.ImpuestosTableAdapter
    Friend WithEvents TableAdapterManager As PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents CodImpuestoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ProcentajeImpuesto1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents ProcentajeImpuesto2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents CreditosFiscalesHijoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CreditosFiscalesConyugeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ArchivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegesarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GuardarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ModificarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MostarHistorialToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents EliminarToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripButton6 As System.Windows.Forms.ToolStripButton
End Class
