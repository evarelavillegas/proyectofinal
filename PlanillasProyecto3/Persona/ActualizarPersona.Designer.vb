﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ActualizarPersona
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnActNombre = New System.Windows.Forms.Button()
        Me.btnActuCedula = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnActNombre
        '
        Me.btnActNombre.Location = New System.Drawing.Point(333, 89)
        Me.btnActNombre.Name = "btnActNombre"
        Me.btnActNombre.Size = New System.Drawing.Size(75, 35)
        Me.btnActNombre.TabIndex = 40
        Me.btnActNombre.Text = "Actualizar Por &Nombre"
        Me.btnActNombre.UseVisualStyleBackColor = True
        '
        'btnActuCedula
        '
        Me.btnActuCedula.Location = New System.Drawing.Point(188, 87)
        Me.btnActuCedula.Name = "btnActuCedula"
        Me.btnActuCedula.Size = New System.Drawing.Size(75, 37)
        Me.btnActuCedula.TabIndex = 39
        Me.btnActuCedula.Text = "&Actualizar Por Cedula"
        Me.btnActuCedula.UseVisualStyleBackColor = True
        '
        'ActualizarPersona
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(698, 261)
        Me.Controls.Add(Me.btnActNombre)
        Me.Controls.Add(Me.btnActuCedula)
        Me.Name = "ActualizarPersona"
        Me.Text = "ActualizarPersona"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnActNombre As System.Windows.Forms.Button
    Friend WithEvents btnActuCedula As System.Windows.Forms.Button
End Class
