﻿Public Class frmCalcularVaciones

    Private Sub VacacionesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.VacacionesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmCalcularVaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.DatosPersonales' Puede moverla o quitarla según sea necesario.
        Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.Vacaciones' Puede moverla o quitarla según sea necesario.
        'Me.VacacionesTableAdapter.Fill(Me.Planilla2DataSet.Vacaciones)

    End Sub



    Sub Calcular()
        For i As Integer = 0 To DatosPersonalesDataGridView.Rows.Count - 2

            Dim fecha As String = DatosPersonalesDataGridView.Rows(i).Cells(7).Value.ToString()
            Dim fechaIngreso As Date = Convert.ToDateTime(fecha)
            Dim canSemanas As Integer = DateDiff(DateInterval.Weekday, fechaIngreso, Date.Now())
            Dim canMeses As Integer = DateDiff(DateInterval.Month, fechaIngreso, Date.Now())
            Dim canAños As Integer = DateDiff(DateInterval.Year, fechaIngreso, Date.Now())

            Try
                If canSemanas > 50 Then

                    If (canAños < 5) Then
                        canMeses *= 1.25
                        Me.VacacionesTableAdapter.InsertVacaciones(DatosPersonalesDataGridView.Rows(i).Cells(3).Value.ToString(), canMeses, DatosPersonalesDataGridView.Rows(i).Cells(0).Value.ToString())
                        Me.VacacionesTableAdapter.Fill(Me.Planilla2DataSet.Vacaciones)
                    ElseIf (canAños >= 5 Or canAños < 10) Then
                        canMeses *= 1.88
                        Me.VacacionesTableAdapter.InsertVacaciones(DatosPersonalesDataGridView.Rows(i).Cells(3).Value.ToString(), canMeses, DatosPersonalesDataGridView.Rows(i).Cells(0).Value.ToString())
                        Me.VacacionesTableAdapter.Fill(Me.Planilla2DataSet.Vacaciones)
                    ElseIf (canAños >= 10) Then
                        canMeses *= 2.5
                        Me.VacacionesTableAdapter.InsertVacaciones(DatosPersonalesDataGridView.Rows(i).Cells(3).Value.ToString(), canMeses, DatosPersonalesDataGridView.Rows(i).Cells(0).Value.ToString())
                        Me.VacacionesTableAdapter.Fill(Me.Planilla2DataSet.Vacaciones)

                    End If
                Else
                    Me.VacacionesTableAdapter.UpdateVacaciones(DatosPersonalesDataGridView.Rows(i).Cells(3).Value.ToString(), "No a Cumplido", DatosPersonalesDataGridView.Rows(i).Cells(0).Value.ToString(), DatosPersonalesDataGridView.Rows(i).Cells(0).Value.ToString())
                    Me.VacacionesTableAdapter.Fill(Me.Planilla2DataSet.Vacaciones)
                End If
            Catch ex As Exception
                Me.VacacionesTableAdapter.UpdateVacaciones(DatosPersonalesDataGridView.Rows(i).Cells(3).Value.ToString(), canMeses, DatosPersonalesDataGridView.Rows(i).Cells(0).Value.ToString(), DatosPersonalesDataGridView.Rows(i).Cells(0).Value.ToString())
                Me.VacacionesTableAdapter.Fill(Me.Planilla2DataSet.Vacaciones)
            End Try

        Next
    End Sub


    Private Sub btnCalcular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalcular.Click

        Calcular()

    End Sub


    Private Sub frmCalcularVaciones_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.MouseLeave, MyBase.MouseLeave, btnCalcular.MouseLeave, ArchivoToolStripMenuItem.MouseLeave, AccionesToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub btnCalcular_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalcular.MouseHover
        ToolStripStatusLabel1.Text = "Calcula Las Vacaciones De todo Los Empleados"
    End Sub

    Private Sub ToolStripButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.Click
        Me.Hide()
        frmPrincipal.Show()
    End Sub

    Private Sub CalcularToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CalcularToolStripMenuItem.Click
        Calcular()
    End Sub

 
    Private Sub RegesarToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Regresar A la Pagina Principal"
    End Sub

    Private Sub CalcularToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CalcularToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Calcula Las Vacaciones De todo Los Empleados"
    End Sub

    Private Sub ToolStripButton6_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.MouseHover
        ToolStripStatusLabel1.Text = "Regresar A la Pagina Principal"
    End Sub

    Private Sub ToolStripButton1_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.MouseHover
        ToolStripStatusLabel1.Text = "Calcula Las Vacaciones De todo Los Empleados"
    End Sub
End Class