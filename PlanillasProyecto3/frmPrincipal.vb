﻿Public Class frmPrincipal

    Public Shared nomForm As String
    Public Shared tipoOpcionDigitada As String
    Public Shared contra As String

    Private Sub IngresarDatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresarDatosToolStripMenuItem.Click

        '  ModulePlanilla.nomForm = New String("Guardar")
        nomForm = "Guardar"
        formdatosEmpleadosActualizar.btnActualizar.Text = "&Guardar"
        formdatosEmpleadosActualizar.btnActualizar.Image = My.Resources.guardar_documento_icono_7840_48
        Me.Hide()
        formdatosEmpleadosActualizar.Show()
        formdatosEmpleadosActualizar.btnBuscar.Enabled = False
        formdatosEmpleadosActualizar.btnConsultar.Enabled = False
        formdatosEmpleadosActualizar.txtDatoConsulta.Enabled = False
        formdatosEmpleadosActualizar.ToolStripButton2.Enabled = False
        formdatosEmpleadosActualizar.ActulizarToolStripMenuItem.Enabled = False

    End Sub

    Private Sub ConsultarDatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarDatosToolStripMenuItem.Click
        Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)
        Me.Hide()
        frmDatosEmpleadoConsulta.Show()
    End Sub

    Private Sub IngresarPuestosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresarPuestosToolStripMenuItem.Click
        Me.Hide()
        nomForm = "Guardar"
        frmPuestosActualizar.btnBuscar.Enabled = False
        frmPuestosActualizar.btnConsultar.Enabled = False
        frmPuestosActualizar.txtDatoConsulta.Enabled = False
        frmPuestosActualizar.Show()
    End Sub

    Private Sub ConsultarPuestosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarPuestosToolStripMenuItem.Click
        Me.Hide()
        frmPuestosConsulta.Show()
    End Sub

    Private Sub IngresarAjustesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresarAjustesToolStripMenuItem.Click
        Me.Hide()
        frmAjustes.Show()
    End Sub

    Private Sub ConsultarAjustesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarAjustesToolStripMenuItem.Click
        Me.Hide()
        nomForm = "Consultar"
        frmConsulataAjustes.btnBuscar.Text = "&Buscar"
        frmConsulataAjustes.Show()

    End Sub

    Private Sub IngresarDeduccionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresarDeduccionesToolStripMenuItem.Click
        Me.Hide()
        frmDeducciones.Show()
    End Sub

    Private Sub ConsultarDedueccionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarDedueccionesToolStripMenuItem.Click
        Me.Hide()
        frmDeduccionesConsulta.Show()
    End Sub

    Private Sub IngresarPlanillaMensualToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresarPlanillaMensualToolStripMenuItem.Click
        Me.Hide()
        frmPlanillaMensuals.Show()
    End Sub

    Private Sub ConsultarPlanillaMensualToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarPlanillaMensualToolStripMenuItem.Click
        Me.Hide()
        frmPlanillaMensualConsultaS.Show()
    End Sub

    Private Sub IngresarPlanillaBisemanalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresarPlanillaBisemanalToolStripMenuItem.Click
        Me.Hide()
        frmPlanillaBisemanal.Show()
    End Sub
    Private Sub ConsultarPlanillaBisemanalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarPlanillaBisemanalToolStripMenuItem.Click
        Me.Hide()
        frmPlanillaBisemanalConsulta.Show()
    End Sub

    Private Sub DeduccucionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeduccucionesToolStripMenuItem.Click

    End Sub

    Private Sub frmPrincipal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.UsuarioRegistro' Puede moverla o quitarla según sea necesario.
        Me.UsuarioRegistroTableAdapter.Fill(Me.Planilla2DataSet.UsuarioRegistro)
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.DatosPersonales' Puede moverla o quitarla según sea necesario.
        Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)
        'TODO: This line of code loads data into the 'Planilla2DataSet.Impuestos' table. You can move, or remove it, as needed.
        Me.ImpuestosTableAdapter.Fill(Me.Planilla2DataSet.Impuestos)

    End Sub

    Private Sub ActualizarDatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActualizarDatosToolStripMenuItem.Click
        Me.Hide()
        nomForm = "Actualizar"
        formdatosEmpleadosActualizar.btnActualizar.Text = "&Actualizar"
        formdatosEmpleadosActualizar.btnActualizar.Image = My.Resources.guardar_documento_icono_7840_48
        formdatosEmpleadosActualizar.Show()
        formdatosEmpleadosActualizar.btnBuscar.Enabled = True
        formdatosEmpleadosActualizar.btnConsultar.Enabled = True
        formdatosEmpleadosActualizar.txtDatoConsulta.Enabled = True
        formdatosEmpleadosActualizar.ToolStripButton2.Enabled = True
        formdatosEmpleadosActualizar.ActulizarToolStripMenuItem.Enabled = True
        formdatosEmpleadosActualizar.GuardarToolStripMenuItem.Enabled = False
        formdatosEmpleadosActualizar.ToolStripButton1.Enabled = False


    End Sub

    Private Sub EliminarDatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarDatosToolStripMenuItem.Click
        Me.Hide()
        frmDatosEmpleadoEliminar.Show()
    End Sub

    Private Sub ActualizarPuestoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActualizarPuestoToolStripMenuItem.Click
        Me.Hide()
        nomForm = "Actualizar"
        frmPuestosActualizar.btnBuscar.Enabled = True
        frmPuestosActualizar.btnConsultar.Enabled = True
        frmPuestosActualizar.txtDatoConsulta.Enabled = True
        frmPuestosActualizar.Show()
    End Sub

    Private Sub EliminarPuestoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarPuestoToolStripMenuItem.Click
        Me.Hide()
        frmPuestoEliminar.Show()
    End Sub

    Private Sub AgregarUsuarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgregarUsuarioToolStripMenuItem.Click
        OperacionUsuario("Ingresar")
    End Sub

    Private Sub EliminarUsuarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarUsuarioToolStripMenuItem.Click
        OperacionUsuario("Eliminar")
    End Sub

    Private Sub ActualizarUsuarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActualizarUsuarioToolStripMenuItem.Click
        OperacionUsuario("Actualizar")
    End Sub

    Private Sub IngresarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresarToolStripMenuItem.Click
        Me.Hide()
        frmImpuesto.Show()
    End Sub

    Private Sub ConsultarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarToolStripMenuItem.Click
        Me.Hide()
        frmConsultaImpuesto.Show()
    End Sub

    Private Sub ImpuestosBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.ImpuestosBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub EliminarAjusteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarAjusteToolStripMenuItem.Click
        nomForm = "Eliminar"
        frmConsulataAjustes.btnBuscar.Text = "&Eliminar"
        frmConsulataAjustes.Show()
    End Sub

    Sub OperacionUsuario(ByVal TipoOpcion As String)


        Select Case TipoOpcion

            Case "Ingresar"
                Me.Hide()
                formOpciones_Usuario.btnOperacion.Text = "&Guardar Usuario"
                formOpciones_Usuario.btnConsultar.Visible = True
                formOpciones_Usuario.Show()
                tipoOpcionDigitada = "Ingresar"

            Case "Actualizar"
                Me.Hide()
                contra = InputBox("Codigo", "Puesto a actualizar")

                If Me.UsuarioRegistroTableAdapter.BuscarCodUsuario(Me.Planilla2DataSet.UsuarioRegistro, contra) = "0" Then
                    If MsgBox("El Codigo No pertenese a ningun Usuario, Desea Ir A la Ventana de Consulta ", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                        frmConsultaUsuario.Show()
                        Me.Hide()
                    Else
                        Me.Show()
                    End If
                End If

            Case "Eliminar"
                Me.Hide()
                formOpciones_Usuario.btnOperacion.Text = "&Eliminar Usuario"
                formOpciones_Usuario.btnConsultar.Visible = True
                formOpciones_Usuario.Show()
                tipoOpcionDigitada = "Eliminar"

            Case Else

        End Select


    End Sub


 


    Private Sub ConsultarUsuarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarUsuarioToolStripMenuItem.Click
        Me.Hide()
        frmConsultaUsuario.Show()
    End Sub

    Private Sub ReporteEmpleadosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReporteEmpleadosToolStripMenuItem.Click
        frmReporteEmpleado.Show()
    End Sub

    Private Sub ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem1.Click
        frmReportePuestos.Show()
    End Sub

    Private Sub ToolStripMenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem3.Click
        formReporteAguinaldo.Show()
    End Sub

    Private Sub ReporteVacacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReporteVacacionesToolStripMenuItem.Click
        frmReporteVaca.Show()

    End Sub

    Private Sub AyudaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AyudaToolStripMenuItem.Click

    End Sub

    Private Sub CalculoDeVacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CalculoDeVacionesToolStripMenuItem.Click
        Me.Hide()
        frmCalcularVaciones.Show()
    End Sub

    Private Sub ConsultaDeVacacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultaDeVacacionesToolStripMenuItem.Click
        Me.Hide()
        frmConsultarVaciones.Show()
    End Sub

    Private Sub CalcularAguinaldoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CalcularAguinaldoToolStripMenuItem.Click
        Me.Hide()
        frmCalcularAguinaldo.Show()
    End Sub

    Private Sub ConsultarAguinaldoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarAguinaldoToolStripMenuItem.Click
        Me.Hide()
        frmConsultarAguinaldo.Show()
    End Sub
End Class